import AssemblyKeys._

name := "xivo-queue-stats"

version := "1.2.1"

scalaVersion := "2.10.3"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

libraryDependencies ++= Seq("org.scalatest" % "scalatest_2.10" % "2.0" % "test",
                            "postgresql" % "postgresql" % "9.1-901.jdbc3",
                            "play" % "anorm_2.10" % "2.1.5",
                            "joda-time" % "joda-time" % "2.0",
                            "org.joda" % "joda-convert" % "1.2",
                            "org.dbunit" % "dbunit" % "2.4.7" % "test",
                            "org.easymock" % "easymock" % "3.1" % "test",
                            "ch.qos.logback" % "logback-classic" % "1.0.7",
                            "org.rogach" %% "scallop" % "0.9.5",
                            "commons-configuration" % "commons-configuration" % "1.7")

parallelExecution in Test := false

publishArtifact in (Compile, packageDoc) := false

mainClass in Compile := Some("xivo.queuestats.Main")

assemblySettings

mergeStrategy in assembly <<= (mergeStrategy in assembly) {
  (old) => {
    case PathList("org", "apache", "commons", xs@_*) => MergeStrategy.first
    case x => old(x)
  }
}

jarName in assembly := "xivo-queue-stats.jar"

test in assembly := {}

