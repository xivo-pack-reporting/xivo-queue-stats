package xivo.queuestats.model


import java.sql.Connection
import anorm._
import anorm.SqlParser._

object Cel {
  def linkedIdFromUniqueId(uniqueid: String)(implicit c: Connection): Option[String] = SQL("SELECT linkedid FROM cel WHERE uniqueid = {uniqueid}")
    .on('uniqueid -> uniqueid).as(get[String]("linkedid") *).headOption
}
