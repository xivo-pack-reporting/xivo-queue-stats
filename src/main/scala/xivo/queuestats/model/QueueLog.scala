package xivo.queuestats.model

import java.sql.Connection
import java.util.Date
import anorm._
import anorm.SqlParser._
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat

case class QueueLog(id: Int, time: Date, callid: String, queueName: String,
                    agent: String, event: String, data1: Option[String], data2: Option[String],
                    data3: Option[String], data4: Option[String], data5: Option[String])

object QueueLog extends TimedTable("queue_log", "time") {

  val batchSize = 50000

  val callRelatedEvents = List("ENTERQUEUE", "CONNECT", "COMPLETEAGENT", "COMPLETECALLER", "TRANSFER", "ABANDON",
  "EXITWITHTIMEOUT", "CLOSED", "DIVERT_CA_RATIO", "DIVERT_HOLDTIME", "FULL", "LEAVEEMPTY", "JOINEMPTY", "RINGNOANSWER")

  val loginEvents = List("AGENTCALLBACKLOGIN", "AGENTCALLBACKLOGOFF")

  val agentRelatedEvents = loginEvents ++ List("WRAPUPSTART", "PAUSEALL", "UNPAUSEALL")

  val queueLogColumns = "id, left(time, 19) AS time, callid, queuename, agent, event, data1, data2, data3, data4, data5"

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")

  def agentEventsByAscendingId(start: Date, end: Date)(implicit conn: Connection): List[QueueLog] = {
    val eventsClause = SqlUtils.createInClauseOrTrue("event", agentRelatedEvents)
    SQL(s"""SELECT $queueLogColumns FROM queue_log WHERE $eventsClause AND time >= {start} AND time < {end} ORDER BY time ASC""")
      .on('start -> new DateTime(start).toString(format),'end -> new DateTime(end).toString(format))
      .as(QueueLog.simple *)
  }

  def getMaxQueueLogCallEventsId()(implicit conn: Connection): Option[Int] = {
    val eventsClause =  SqlUtils.createInClauseOrTrue("event", callRelatedEvents)
    val rs = SQL(s"SELECT max(id) AS max_id FROM queue_log WHERE $eventsClause").resultSet()
    rs.next()
    val result = rs.getInt("max_id")
    rs.close()
    if(result == 0) None else Some(result)
  }

  val simple = get[Int]("id") ~
               get[String]("time") ~
               get[String]("callid") ~
               get[String]("queuename") ~
               get[String]("agent") ~
               get[String]("event") ~
               get[Option[String]]("data1") ~
               get[Option[String]]("data2") ~
               get[Option[String]]("data3") ~
               get[Option[String]]("data4") ~
               get[Option[String]]("data5")  map {
      case id ~ time ~ callid ~ queueName ~ agent ~ event ~ data1 ~ data2 ~ data3 ~ data4 ~ data5 =>
        QueueLog(id, format.parseDateTime(time).toDate, callid, queueName, agent, event, data1, data2, data3, data4, data5)
    }

  def callEventsByAscendingId(startId: Int, pendingCids: List[String])(implicit conn: Connection): List[QueueLog] = {
    val eventsClause = SqlUtils.createInClauseOrTrue("event", callRelatedEvents)
    val pendingsCidsClause = SqlUtils.createInClauseOrFalse("callid", pendingCids)

    SQL( s"""SELECT $queueLogColumns FROM queue_log WHERE (id >= {startId} OR $pendingsCidsClause)
            AND $eventsClause ORDER BY id ASC LIMIT $batchSize""").on('startId -> startId).as(QueueLog.simple *)
  }

  def loginActionsSince(time: DateTime)(implicit c: Connection): List[QueueLog] =
    SQL(s"SELECT $queueLogColumns FROM queue_log WHERE time > {time} AND ${SqlUtils.createInClauseOrFalse("event", loginEvents)} ORDER BY id ASC")
    .on('time -> time.toDate).as(simple *)

}
