package xivo.queuestats.model

import java.sql.Connection
import java.util.Date

import org.joda.time.DateTime
import anorm._
import anorm.SqlParser._

case class AgentPosition(agent: String, lineNumber: String, sda: Option[String], startTime: DateTime, endTime: Option[DateTime])

object AgentPosition {
  def sdaFromLineNum(line: String)(implicit c: Connection): Option[String] =
    SQL("""SELECT e1.exten AS exten FROM extensions e1 JOIN dialaction d ON e1.typeval = d.categoryval JOIN extensions e2 ON e2.typeval = d.actionarg1
        WHERE e1.type = 'incall' AND d.category = 'incall' AND d.action = 'user' AND e2.type = 'user' AND e2.exten = {line}""")
    .on('line -> line).as(get[String]("exten") *).headOption

  def removePendingLoginForAgents(agents: List[String])(implicit c: Connection) =
    SQL(s"DELETE FROM agent_position WHERE end_time IS NULL AND ${SqlUtils.createInClauseOrFalse("agent_num", agents)}").executeUpdate()


  val simple = get[String]("line_number") ~
    get[Option[String]]("sda") ~
    get[String]("agent_num") ~
    get[Date]("start_time") ~
    get[Option[Date]]("end_time")  map {
    case line ~ sda ~ agent ~ start ~ end => AgentPosition(agent, line, sda, new DateTime(start), end.map(d => new DateTime(d)))
  }

  def lastPendingForAgent(agent: String)(implicit c: Connection): Option[AgentPosition] =
    SQL("SELECT line_number, sda, agent_num, start_time, end_time FROM agent_position WHERE agent_num = {agent} AND end_time IS NULL ORDER BY start_time DESC LIMIT 1")
      .on('agent -> agent).as(simple *).headOption

  def getMaxDate()(implicit c: Connection): Option[DateTime] = SQL("SELECT greatest(max(start_time), max(end_time)) AS res FROM agent_position").single(
    get[Option[Date]]("res") map {
      case date => date.map(d => new DateTime(d))
    })

  def insert(position: AgentPosition)(implicit c: Connection): Unit =
    SQL("INSERT INTO agent_position(line_number, sda, agent_num, start_time, end_time) VALUES ({line}, {sda}, {agent}, {start}, {end})")
      .on('line -> position.lineNumber, 'sda -> position.sda, 'agent -> position.agent, 'start -> position.startTime.toDate, 'end -> position.endTime.map(_.toDate))
      .executeUpdate()
}
