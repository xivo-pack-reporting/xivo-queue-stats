package xivo.queuestats.model

import java.util.Date
import org.joda.time.{Period, DateTime}
import anorm.SQL
import java.sql.Connection

case class StatQueuePeriodic(id: Option[Int], time: Date, queue: String, answered: Int, abandoned: Int, total: Int, full: Int,
                             closed: Int, joinEmpty: Int, leaveEmpty: Int, divertCaRatio: Int, divertWaitTime: Int, timeout: Int) {

  override def equals(o: Any): Boolean = {
    if(o.isInstanceOf[StatQueuePeriodic]) {
      val that = o.asInstanceOf[StatQueuePeriodic]
      return (that.id, new DateTime(that.time), that.queue, that.answered, that.abandoned, that.total, that.full, that.closed,
        that.joinEmpty, that.leaveEmpty, that.divertCaRatio, that.divertWaitTime, that.timeout) ==
        (id, new DateTime(time), queue, answered, abandoned, total, full, closed, joinEmpty, leaveEmpty, divertCaRatio, divertWaitTime, timeout)
    } else false
  }

}

object StatQueuePeriodic extends TimedTable("stat_queue_periodic", "time") {

  def updateFormerStats(call: CallOnQueue, interval: Period)(implicit conn: Connection) = {
    if(call.status != Some(CallExitType.Answered)) {
      val column = call.status.get.toString
      SQL(s"UPDATE stat_queue_periodic SET $column = $column + 1 WHERE queue = {queue} AND time = round_to_x_seconds({time}, {seconds})")
        .on('queue -> call.queueRef, 'time -> call.queueTime, 'seconds -> interval.toStandardSeconds.getSeconds)
        .executeUpdate
    }
  }

  def insertFromCallOnQueue(start: DateTime, end: DateTime, interval: Period)(implicit conn: Connection) = {
    SQL("""INSERT INTO stat_queue_periodic(time, queue, answered, abandoned, total, "full", closed, joinempty, leaveempty,
                  divert_ca_ratio, divert_waittime, timeout)
          (SELECT the_time, queue_ref,
                  coalesce(t2.answered, 0),
                  coalesce(t1.abandoned, 0),
                  coalesce(t1.total, 0),
                  coalesce(t1."full", 0),
                  coalesce(t1.closed, 0),
                  coalesce(t1.joinempty, 0),
                  coalesce(t1.leaveempty, 0),
                  coalesce(t1.divert_ca_ratio, 0),
                  coalesce(t1.divert_waittime, 0),
                  coalesce(t1.timeout, 0)
          FROM
                  (SELECT round_to_x_seconds(queue_time, {seconds}) AS the_time, queue_ref,
                          sum(CASE WHEN status = 'abandoned' THEN 1 ELSE 0 END) AS abandoned,
                          count(*) AS total,
                          sum(CASE WHEN status = 'full' THEN 1 ELSE 0 END) AS "full",
                          sum(CASE WHEN status = 'closed' THEN 1 ELSE 0 END) AS closed,
                          sum(CASE WHEN status = 'joinempty' THEN 1 ELSE 0 END) AS joinempty,
                          sum(CASE WHEN status = 'leaveempty' THEN 1 ELSE 0 END) AS leaveempty,
                          sum(CASE WHEN status = 'divert_ca_ratio' THEN 1 ELSE 0 END) AS divert_ca_ratio,
                          sum(CASE WHEN status = 'divert_waittime' THEN 1 ELSE 0 END) AS divert_waittime,
                          sum(CASE WHEN status = 'timeout' THEN 1 ELSE 0 END) AS timeout
                  FROM call_on_queue WHERE queue_time >= {start} AND queue_time < {end}
                  GROUP BY the_time, queue_ref) t1
          NATURAL FULL JOIN
                  (SELECT round_to_x_seconds(answer_time, {seconds}) AS the_time, queue_ref, count(*) AS answered
                  FROM call_on_queue WHERE answer_time >= {start} AND answer_time < {end} AND status = 'answered'
                  GROUP BY the_time, queue_ref) t2
          )""")
      .on('seconds -> interval.toStandardSeconds.getSeconds, 'start -> start.toDate, 'end -> end.toDate)
      .executeUpdate()
  }

}
