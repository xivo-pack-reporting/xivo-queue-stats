package xivo.queuestats.model

import java.util.Date
import anorm.{~, SQL}
import java.sql.Connection
import anorm.SqlParser._
import org.joda.time.DateTime
import xivo.queuestats.model.CallExitType.CallExitType
import xivo.queuestats.model.SqlUtils.optionToValue

object CallExitType extends Enumeration {

  type CallExitType = Value
  val Full = Value("full")
  val Closed = Value("closed")
  val JoinEmpty = Value("joinempty")
  val LeaveEmpty = Value("leaveempty")
  val DivertCaRatio = Value("divert_ca_ratio")
  val DivertWaitTime = Value("divert_waittime")
  val Answered = Value("answered")
  val Abandoned = Value("abandoned")
  val Timeout = Value("timeout")
}

case class CallOnQueue(var id: Option[Int], var callid: String, var queueTime: Date, var ringSeconds: Int, var answerTime: Option[Date], var hangupTime: Option[Date],
                           var status: Option[CallExitType], var queueRef: String, var agentNum: Option[String]) {
  override def equals(o: Any): Boolean = {
    if(o.isInstanceOf[CallOnQueue]) {
      val call = o.asInstanceOf[CallOnQueue]
      return (id, callid, new DateTime(queueTime), ringSeconds, if(answerTime == None) None else new DateTime(answerTime.get),
        if(hangupTime == None) None else new DateTime(hangupTime.get), status, queueRef, agentNum) ==
        (call.id, call.callid, new DateTime(call.queueTime), call.ringSeconds, if(call.answerTime == None) None else new DateTime(call.answerTime.get),
          if(call.hangupTime == None) None else new DateTime(call.hangupTime.get), call.status, call.queueRef, call.agentNum)
    }
    else
      return false
  }
}

object CallOnQueue extends TimedTable("call_on_queue", "queue_time") {

  def getStalePendingCalls()(implicit conn: Connection): List[CallOnQueue] = {
    val rsDate = SQL("SELECT max(queue_time) FROM call_on_queue").resultSet()
    if(rsDate.next) {
      val date = rsDate.getTimestamp(1)
      return SQL(
        """SELECT callid, queue_time, total_ring_seconds, answer_time, hangup_time, status::varchar AS status, queue_ref, agent_num
          FROM call_on_queue WHERE hangup_time IS NULL AND queue_time < {maxDate}::timestamp - INTERVAL '1 day'""").on('maxDate -> date).as(simple *)
    }
    List()
  }


  val simple = get[String]("callid") ~ get[Date]("queue_time") ~ get[Int]("total_ring_seconds") ~
  get[Option[Date]]("answer_time") ~ get[Option[Date]]("hangup_time") ~ get[Option[String]]("status") ~get[String]("queue_ref") ~
  get[Option[String]]("agent_num") map {
    case callid ~ queuetime ~ ringseconds ~ answertime ~ hanguptime ~ status ~ queue ~ agent => {
      val realStatus = status match {
        case None => None
        case Some(str) => Some(CallExitType.withName(str))
      }
      CallOnQueue(None, callid, queuetime, ringseconds, answertime, hanguptime, realStatus, queue, agent)
    }
  }

  def insert(calls: List[CallOnQueue])(implicit conn: Connection) = calls.foreach(call => SQL(
  """INSERT INTO call_on_queue(callid, queue_time, total_ring_seconds, answer_time, hangup_time, status, queue_ref, agent_num)
      VALUES ({callid}, {queue_time}, {total_ring_seconds}, {answer_time}, {hangup_time}, {status}, {queue_ref}, {agent_num})""").on(
  'callid -> call.callid, 'queue_time -> call.queueTime, 'total_ring_seconds -> call.ringSeconds, 'answer_time -> call.answerTime,
  'hangup_time -> call.hangupTime, 'status -> optionToValue(call.status), 'queue_ref -> call.queueRef, 'agent_num -> call.agentNum)
    .executeUpdate())

  def deleteByCallid(callids: List[String])(implicit conn: Connection) = {
    val inClause = SqlUtils.createInClauseOrFalse("callid", callids)
    SQL(s"DELETE FROM call_on_queue WHERE $inClause").executeUpdate()
  }

  def getPendingCalls()(implicit conn: Connection): List[CallOnQueue] =
  SQL(s"""SELECT callid, queue_time, total_ring_seconds, answer_time, hangup_time, status::varchar AS status, queue_ref, agent_num FROM call_on_queue
         WHERE hangup_time IS NULL""").as(simple *)

  def getFormerlyPendingCalls(pendingCalls: List[CallOnQueue])(implicit conn: Connection): List[CallOnQueue] = pendingCalls match {
    case List() => List()
    case head::tail => SQL(s"""SELECT callid, queue_time, total_ring_seconds, answer_time, hangup_time, status::varchar AS status, queue_ref, agent_num FROM call_on_queue
            WHERE queue_time = {queue_time} AND callid = {callid} AND status IS NOT NULL""")
        .on('queue_time -> head.queueTime, 'callid -> head.callid)
        .as(simple *):::getFormerlyPendingCalls(tail)
  }

  def getAllWithNullStatus()(implicit conn: Connection): List[CallOnQueue] = SQL(s"""SELECT callid, queue_time, total_ring_seconds, answer_time, hangup_time, status::varchar AS status, queue_ref, agent_num FROM call_on_queue
         WHERE status IS NULL""").as(simple *)
}
