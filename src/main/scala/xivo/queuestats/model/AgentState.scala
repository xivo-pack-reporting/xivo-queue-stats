package xivo.queuestats.model

import java.sql.Connection
import org.joda.time.DateTime
import anorm.SQL

import scala.collection.mutable

case class AgentState(date: DateTime, state: State.State)


object State extends Enumeration {
  type State = Value
  val LoggedOff = Value("logged_off")
  val LoggedOn = Value("logged_on")
  val Paused = Value("paused")
}

object AgentState {
  def deleteAll()(implicit conn: Connection) = SQL("TRUNCATE agent_states").executeUpdate()


  def insertStates(states: Map[String, AgentState])(implicit conn: Connection) = {
    conn.setAutoCommit(false)
    for((agent, state) <- states) {
      SQL("INSERT INTO agent_states(agent, time, state) VALUES ({agent}, {time}, {state})")
        .on('agent -> agent, 'time -> state.date.toDate, 'state -> state.state.toString)
        .executeUpdate()
    }
    conn.commit()
    conn.setAutoCommit(true)
  }

  def getStates()(implicit conn: Connection): Map[String, AgentState] = {
    val rs = SQL("SELECT agent, time, state FROM agent_states").resultSet()
    val result = mutable.Map[String, AgentState]()
    while(rs.next())
      result.put(rs.getString("agent"), AgentState(new DateTime(rs.getTimestamp("time")), State.withName(rs.getString("state"))))
    rs.close()
    result.toMap
  }
}