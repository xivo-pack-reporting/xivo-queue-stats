package xivo.queuestats.model

import anorm._
import org.joda.time.DateTime
import java.sql.Connection

class TimedTable(val table: String, val timeColumn: String) {
  def getMinDate()(implicit conn: Connection): Option[DateTime] = getAggregatedTime("min")

  def getMaxDate()(implicit conn: Connection): Option[DateTime] = getAggregatedTime("max")

  private def getAggregatedTime(aggregateFunction: String)(implicit conn: Connection): Option[DateTime] = {
    val rs = SQL(s"SELECT $aggregateFunction($timeColumn) AS res FROM $table").resultSet()
    var result: Option[DateTime] = None
    if (rs.next() && rs.getTimestamp("res") != null)
      result = Some(new DateTime(rs.getTimestamp("res")))
    rs.close()
    return result
  }

}
