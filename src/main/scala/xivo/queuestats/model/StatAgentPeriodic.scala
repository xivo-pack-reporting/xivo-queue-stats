package xivo.queuestats.model

import java.util.Date

import anorm._
import org.joda.time.DateTime
import java.sql.Connection

case class StatAgentPeriodic(id: Option[Int], time: Date, agent: String, var loginTime: Int, var pauseTime: Int, var wrapupTime: Int) extends Ordered[StatAgentPeriodic] {
  override def compare(that: StatAgentPeriodic): Int = this.toString compare that.toString

  override def equals(o: Any): Boolean = {
    if (o.isInstanceOf[StatAgentPeriodic]) {
      val stat = o.asInstanceOf[StatAgentPeriodic]
      return (id, new DateTime(time), agent, loginTime, pauseTime, wrapupTime) ==
        (stat.id, new DateTime(stat.time), stat.agent, stat.loginTime, stat.pauseTime, stat.wrapupTime)
    }
    else
      return false
  }
}

object StatAgentPeriodic extends TimedTable("stat_agent_periodic", "time") {

  val insertStatement = SQL("""INSERT INTO stat_agent_periodic(time, agent, login_time, pause_time, wrapup_time) VALUES
      ({time}, {agent}, {loginTime} * INTERVAL '1 second', {pauseTime} * INTERVAL '1 second', {wrapupTime} * INTERVAL '1 second')""")

  def insertAll(stats: Iterable[StatAgentPeriodic])(implicit conn: Connection) {
    conn.setAutoCommit(false)
    stats.foreach(s => insert(s))
    conn.commit()
    conn.setAutoCommit(true)
  }

  private def insert(stat: StatAgentPeriodic)(implicit conn: Connection) =
    insertStatement.on('time -> stat.time, 'agent -> stat.agent, 'loginTime -> stat.loginTime, 'pauseTime -> stat.pauseTime, 'wrapupTime -> stat.wrapupTime)
      .executeUpdate()

}