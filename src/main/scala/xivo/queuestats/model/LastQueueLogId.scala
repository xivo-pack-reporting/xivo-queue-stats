package xivo.queuestats.model

import java.sql.Connection

import anorm._

object LastQueueLogId {
  def setId(id: Int)(implicit connection: Connection) = {
    var notEmpty = SQL("SELECT id FROM last_queue_log_id").resultSet.next()
    if(notEmpty)
      SQL("UPDATE last_queue_log_id SET id = {id}").on('id -> id).executeUpdate
    else
      SQL("INSERT INTO last_queue_log_id(id) VALUES ({id})").on('id -> id).executeUpdate
  }

  def getId()(implicit connection: Connection): Int = {
    var id = 0
    var rs = SQL("SELECT id FROM last_queue_log_id").resultSet
    if(rs.next) {
      id = rs.getInt("id")
    }
    rs.close
    return id
  }
}