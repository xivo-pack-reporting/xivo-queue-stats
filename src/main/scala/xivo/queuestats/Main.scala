package xivo.queuestats

import org.apache.commons.configuration.PropertiesConfiguration
import org.joda.time.format.DateTimeFormat
import org.rogach.scallop.ScallopConf
import org.slf4j.LoggerFactory

object Main {

  val ConfigFile = "/etc/xivo-queue-stats/xivo-queue-stats.properties"
  val logger = LoggerFactory.getLogger(getClass)
  val config = new PropertiesConfiguration(ConfigFile)
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")


  def main(args: Array[String]): Unit = {
    val opts = parseArgs(args)
    val period = TimeUtils.parseInterval(config.getString("reporting.interval"))
    val connection = new ConnectionFactory(config.getString("reporting.address"),
      config.getString("reporting.database.name"),
      config.getString("reporting.database.user"),
      config.getString("reporting.database.password")).getConnection()
    new XivoStat(connection, period, opts.start.get.map(s => format.parseDateTime(s)), opts.end.get.map(s => format.parseDateTime(s))).calculateStats()
  }

  def parseArgs(args: Array[String]) = {
    val res = new ScallopConf(args) {
      banner("Usage : xivo-queue-stats [options]")
      val help = opt[Boolean]("help", descr = "Affiche ce message")
      val start = opt[String]("start", descr = "Date de début, au format YYYY-MM-dd HH:mm:ss")
      val end = opt[String]("end", descr = "Date de fin, au format YYYY-MM-dd HH:mm:ss")
    }
    if(res.help.isSupplied) {
      res.printHelp()
      System.exit(0)
    }
    res
  }

}
