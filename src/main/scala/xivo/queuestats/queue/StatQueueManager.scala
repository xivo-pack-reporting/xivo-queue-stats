package xivo.queuestats.queue

import java.sql.Connection

import org.joda.time.{DateTime, Period}
import xivo.queuestats.BoundedManager
import xivo.queuestats.model._

class StatQueueManager(interval: Period, _startTime: Option[DateTime], _endTime: Option[DateTime])(implicit conn: Connection) extends BoundedManager[CallOnQueue.type, StatQueuePeriodic.type] (interval, CallOnQueue, StatQueuePeriodic) {

  override lazy val startTime = _startTime.getOrElse(super.startTime)
  override lazy val endTime = _endTime.getOrElse(super.endTime)

  def insertNewStatQueuePeriodic() =
    StatQueuePeriodic.insertFromCallOnQueue(startTime, endTime, interval)

  def updateFormerStatQueuePeriodic(oldPendingCalls: List[CallOnQueue]) =
    CallOnQueue.getFormerlyPendingCalls(oldPendingCalls).foreach(call => StatQueuePeriodic.updateFormerStats(call, interval))
}
