package xivo.queuestats.parser

import org.slf4j.LoggerFactory
import xivo.queuestats.model.{CallOnQueue, QueueLog}

trait CallState {
  var call: CallOnQueue = null
  val logger = LoggerFactory.getLogger(getClass)

  def getCallOnQueue: Option[CallOnQueue] = Some(call)

  def parseQueueLog(ql: QueueLog): CallState

  protected def processUnexpectedQueueLog(ql: QueueLog) =  {
    logger.info(s"Unexpected queue log $ql on state $this")
    throw new UnexpectedQueueLogException(ql)
  }
}

class UnexpectedQueueLogException(ql: QueueLog) extends Exception

class NoSuchCelException(uniqueId: String) extends Exception {
  override def getMessage: String = s"Could not find a cel with the uniqueid $uniqueId"
}