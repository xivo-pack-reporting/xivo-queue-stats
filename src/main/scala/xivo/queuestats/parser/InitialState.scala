package xivo.queuestats.parser

import java.util.Date
import java.sql.Connection
import xivo.queuestats.model.{CallExitType, CallOnQueue, Cel, QueueLog}

class InitialState(implicit c: Connection) extends CallState {

  call = CallOnQueue(None, "", new Date, 0, None, None, None, "", None)

  override def parseQueueLog(ql: QueueLog): CallState = {
    call.queueTime = ql.time
    call.queueRef = ql.queueName
    call.callid = Cel.linkedIdFromUniqueId(ql.callid) match {
      case None => throw new NoSuchCelException(ql.callid)
      case Some(linkedId) => linkedId
    }

    ql.event match {
      case "ENTERQUEUE" => new Queued(call)

      case "CLOSED" => {
        call.status = Some(CallExitType.Closed)
        call.hangupTime = Some(ql.time)
        new Finished(call)
      }

      case "FULL" => {
        call.status = Some(CallExitType.Full)
        call.hangupTime = Some(ql.time)
        new Finished(call)
      }

      case "DIVERT_CA_RATIO" => {
        call.status = Some(CallExitType.DivertCaRatio)
        call.hangupTime = Some(ql.time)
        new Finished(call)
      }

      case "DIVERT_HOLDTIME" => {
        call.status = Some(CallExitType.DivertWaitTime)
        call.hangupTime = Some(ql.time)
        new Finished(call)
      }

      case "JOINEMPTY" => {
        call.status = Some(CallExitType.JoinEmpty)
        call.hangupTime = Some(ql.time)
        new Finished(call)
      }

      case _ => processUnexpectedQueueLog(ql)
    }

  }
}
