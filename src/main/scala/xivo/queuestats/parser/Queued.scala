package xivo.queuestats.parser

import xivo.queuestats.model.{CallExitType, CallOnQueue, QueueLog}

class Queued(_call: CallOnQueue) extends CallState {
  val agentPattern = "Agent/(\\d+)".r

  def extractAgentNumber(s: String): String = {
    agentPattern.findAllIn(s).matchData foreach {
      m => return m.group(1)
    }
    return s
  }

  call = _call
  override def parseQueueLog(ql: QueueLog): CallState = {
    ql.event match {
      case "ABANDON" => {
        call.hangupTime = Some(ql.time)
        call.status = Some(CallExitType.Abandoned)
        return new Finished(call)
      }
      case "EXITWITHTIMEOUT" => {
        call.hangupTime = Some(ql.time)
        call.status = Some(CallExitType.Timeout)
        return new Finished(call)
      }
      case "CONNECT" => {
        call.answerTime = Some(ql.time)
        call.ringSeconds += ql.data3.get.toInt
        call.status = Some(CallExitType.Answered)
        call.agentNum = Some(extractAgentNumber(ql.agent))
        return new Answered(call)
      }
      case "LEAVEEMPTY" => {
        call.hangupTime = Some(ql.time)
        call.status = Some(CallExitType.LeaveEmpty)
        return new Finished(call)
      }
      case "RINGNOANSWER" => {
        call.ringSeconds += ql.data1.get.toInt / 1000
        this
      }
      case _ => processUnexpectedQueueLog(ql)
    }

  }
}
