package xivo.queuestats.parser

import xivo.queuestats.model.{CallOnQueue, QueueLog}

class Answered(_call: CallOnQueue) extends CallState {
  call = _call
  override def parseQueueLog(ql: QueueLog): CallState = ql.event match {
    case "COMPLETEAGENT" | "COMPLETECALLER" => {
      call.hangupTime = Some(ql.time)
      new Finished(call)
    }
    case "TRANSFER" => {
      call.hangupTime = Some(ql.time)
      new Finished(call)
    }
    case _ => processUnexpectedQueueLog(ql)
  }
}
