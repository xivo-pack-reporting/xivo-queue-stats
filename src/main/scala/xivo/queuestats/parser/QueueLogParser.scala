package xivo.queuestats.parser

import org.slf4j.LoggerFactory
import xivo.queuestats.model.{CallOnQueue, QueueLog}
import scala.collection.mutable.ListBuffer
import scala.collection.mutable
import java.sql.Connection

object QueueLogParser {

  val logger = LoggerFactory.getLogger(getClass)

  def parseAllQueueLogs(logs: List[QueueLog])(implicit c: Connection): List[CallOnQueue] = {
    val groupedQueueLogs: Iterable[Iterable[QueueLog]] = groupQueueLogsByCall(logs)
    groupedQueueLogs.flatMap(list => new QueueLogParser().parseQueueLogs(list)).toList
  }

  def isStartingEvent(event: String): Boolean = List("ENTERQUEUE", "CLOSED", "FULL", "DIVERT_CA_RATIO", "DIVERT_HOLDTIME", "JOINEMPTY") contains event

  private def groupQueueLogsByCall(qls: List[QueueLog]): Iterable[Iterable[QueueLog]] = {
    val callidMap = mutable.Map[String, ListBuffer[ListBuffer[QueueLog]]]()
    qls.foreach(ql => callidMap.get(ql.callid) match {
      case None => callidMap.put(ql.callid, ListBuffer(ListBuffer(ql)))
      case Some(calls) => {
        if(isStartingEvent(ql.event))
          calls.append(ListBuffer(ql))
        else
          calls.last.append(ql)
      }
    })
    callidMap.values.flatten
  }
}

class QueueLogParser(implicit val c: Connection) {

  var callStateMachine = new CallStateMachine()

  def parseQueueLogs(queueLogs: Iterable[QueueLog]): Option[CallOnQueue] = {
    for(ql <- queueLogs) {
      try
        callStateMachine.parseQueueLog(ql)
      catch {
        case e: Exception => QueueLogParser.logger.warn(s"Got an exception while parsing queue log $ql", e)
          return None
      }
    }
    callStateMachine.getCallOnQueue
  }
}
