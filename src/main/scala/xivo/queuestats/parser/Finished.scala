package xivo.queuestats.parser

import xivo.queuestats.model.{CallOnQueue, QueueLog}

class Finished(_call: CallOnQueue) extends CallState {
  call = _call
  override def parseQueueLog(ql: QueueLog): CallState = processUnexpectedQueueLog(ql)
}
