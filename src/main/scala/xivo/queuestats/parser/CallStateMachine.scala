package xivo.queuestats.parser

import xivo.queuestats.model.{CallOnQueue, QueueLog}
import java .sql.Connection

class CallStateMachine(implicit val c: Connection) {

  var state: CallState = new InitialState()

  def getCallOnQueue: Option[CallOnQueue] = state.getCallOnQueue

  @throws(classOf[Exception])
  def parseQueueLog(ql: QueueLog) = state = state.parseQueueLog(ql)

}
