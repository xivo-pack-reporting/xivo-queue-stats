package xivo.queuestats

import org.joda.time.{DateTime, Period}
import xivo.queuestats.model.TimedTable
import java.sql.Connection

abstract class BoundedManager[S <: TimedTable, T <: TimedTable](interval: Period, baseObject: S, calculatedObject: T)(implicit conn: Connection) {
  def startTime: DateTime = calculatedObject.getMaxDate() match {
    case None => baseObject.getMinDate match {
      case None => throw new EmptyTableException(baseObject.table)
      case Some(t) => TimeUtils.roundToInterval(t, interval)
    }
    case Some(t) => t.plus(interval)
  }

  def endTime = TimeUtils.roundToInterval(baseObject.getMaxDate() match {
      case None => throw new EmptyTableException(baseObject.table)
      case Some(t) => t
    }, interval)

}

class EmptyTableException(table: String) extends Exception(s"The table $table is empty")
