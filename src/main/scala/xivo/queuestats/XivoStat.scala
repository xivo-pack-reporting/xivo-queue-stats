package xivo.queuestats

import java.sql.Connection

import org.joda.time.{DateTime, Period}
import org.slf4j.LoggerFactory
import xivo.queuestats.agent.{AgentPositionManager, StatAgentManager}
import xivo.queuestats.model.{CallOnQueue, LastQueueLogId, QueueLog}
import xivo.queuestats.parser.QueueLogParser
import xivo.queuestats.queue.StatQueueManager

class XivoStat(_connection: Connection, interval: Period, startDate: Option[DateTime], endDate: Option[DateTime]) {

  implicit val connection = _connection
  val logger = LoggerFactory.getLogger(getClass)

  def calculateStats() {
    val nonAccountedCalls = CallOnQueue.getAllWithNullStatus()
    generateCallOnQueues
    generateStatAgentPeriodic()
    generateStatQueuePeriodic(nonAccountedCalls)
    generateAgentPositions
  }

  def generateStatQueuePeriodic(nonAccountedCalls: List[CallOnQueue]) {
    try {
      val qManager = new StatQueueManager(interval, startDate, endDate)
      qManager.updateFormerStatQueuePeriodic(nonAccountedCalls)
      qManager.insertNewStatQueuePeriodic()
    } catch {
      case e: EmptyTableException => logger.warn(e.getMessage)
    }
  }

  private def generateStatAgentPeriodic() {
    try {
      new StatAgentManager(interval, startDate, endDate).insertNewStatAgentPeriodic()
    } catch {
      case e: EmptyTableException => logger.warn(e.getMessage)
    }
  }


  def generateCallOnQueues(implicit connection: Connection) = {
    var i = 1
    var lastQlId = LastQueueLogId.getId()
    while (QueueLog.getMaxQueueLogCallEventsId().getOrElse(0) > lastQlId) {
      logger.info(s"Generating call_on_queue, iteration number $i, starting with id ${lastQlId + 1}")
      removeStaleCalls()
      val pendingCallids = CallOnQueue.getPendingCalls().map(call => call.callid)
      logger.info(s"Removing pending calls : $pendingCallids")
      CallOnQueue.deleteByCallid(pendingCallids)
      val queueLogs = QueueLog.callEventsByAscendingId(lastQlId + 1, pendingCallids)
      logger.info("Starting call_on_queue generation")
      val callsOnQueue = QueueLogParser.parseAllQueueLogs(queueLogs)
      CallOnQueue.insert(callsOnQueue)
      lastQlId = queueLogs.last.id
      LastQueueLogId.setId(lastQlId)
      i += 1
    }
    logger.info("call_on_queue generation finished")
  }

  private def removeStaleCalls() {
    val staleCalls = CallOnQueue.getStalePendingCalls().map(c => c.callid)
    if (staleCalls.size > 0) {
      logger.warn(s"Removing stale calls : $staleCalls")
      CallOnQueue.deleteByCallid(staleCalls)
    }
  }

  def generateAgentPositions() {
    try {
      new AgentPositionManager().fillAgentPositions()
    } catch {
      case e: EmptyTableException => logger.warn(e.getMessage)
    }
  }
}
