package xivo.queuestats

import java.util.InvalidPropertiesFormatException

import org.joda.time.{DateTime, Period}

object TimeUtils {

  val intervalStr = "(\\d+):(\\d+):(\\d+)".r

  def roundToInterval(time: DateTime, interval: Period): DateTime = time.minus(new Period((time.getSecondOfDay % interval.toStandardSeconds.getSeconds)*1000))
    .withMillisOfSecond(0)

  def parseInterval(s: String): Period = {
    intervalStr.findAllIn(s).matchData foreach {
      m => return new Period(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2)), Integer.parseInt(m.group(3)), 0)
    }
    throw new InvalidPropertiesFormatException(s"Found : $s, expected : hh:mm:ss")
  }
}
