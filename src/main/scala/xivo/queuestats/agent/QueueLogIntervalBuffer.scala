package xivo.queuestats.agent

import java.security.InvalidParameterException
import java.sql.Connection
import java.util.Date
import scala.collection.mutable.{ListBuffer, MutableList}
import org.joda.time.DateTime
import xivo.queuestats.model.QueueLog

class QueueLogIntervalBuffer(intervals: List[(DateTime, DateTime)])(implicit conn: Connection) extends Iterable[QueueLogInterval] {
  val batchSize = 200
  override def iterator: Iterator[QueueLogInterval] = new QueueLogIntervalIterator(intervals, batchSize, QueueLog.agentEventsByAscendingId(_, _))

}

class QueueLogIntervalIterator(intervals: List[(DateTime, DateTime)], batchSize: Int, fetchMethod: (Date, Date) => List[QueueLog]) extends Iterator[QueueLogInterval] {

  if(batchSize <= 0)
    throw new InvalidParameterException("La taille du tampon doit être strictement positive")
  var remainingIntervals = intervals
  var buffer = MutableList[QueueLogInterval]()
  fillBuffer()

  override def hasNext: Boolean = return remainingIntervals.size > 0 || buffer.size > 0

  override def next(): QueueLogInterval = {
    if(buffer.size == 0)
      fillBuffer()
    val res = buffer.head
    buffer = buffer.tail
    res
  }

  private def fillBuffer(): Unit = {
    if(remainingIntervals.size == 0)
      return
    val (bufferedIntervals, others) = remainingIntervals.splitAt(batchSize)
    remainingIntervals = others
    val qls = fetchMethod(bufferedIntervals.head._1.toDate, bufferedIntervals.last._2.toDate)
    var head::tail = bufferedIntervals
    var qlInt = QueueLogInterval(head._1, head._2, ListBuffer())
    for(ql <- qls) {
      val dTime = new DateTime(ql.time)
      while(! dTime.isBefore(head._2)) {
        buffer += qlInt
        if(tail.isEmpty)
          return
        head = tail.head
        tail = tail.tail
        qlInt = QueueLogInterval(head._1, head._2, ListBuffer())
      }
      qlInt.qls.append(ql)
    }
    buffer += qlInt
  }
}
