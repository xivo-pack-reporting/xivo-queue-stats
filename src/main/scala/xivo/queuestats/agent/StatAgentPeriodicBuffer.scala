package xivo.queuestats.agent

import xivo.queuestats.model.StatAgentPeriodic

import scala.collection.mutable.ListBuffer

class StatAgentPeriodicBuffer(batchSize: Int, insertMethod: Iterable[StatAgentPeriodic] => Unit) {

  val stats = ListBuffer[StatAgentPeriodic]()

  def appendAll(newStats: List[StatAgentPeriodic]) = {
    stats.appendAll(newStats)
    if(stats.size >= batchSize) {
      insertMethod(stats)
      stats.clear()
    }
  }

  def flush() {
    insertMethod(stats)
    stats.clear()
  }
}
