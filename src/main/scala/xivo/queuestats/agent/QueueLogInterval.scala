package xivo.queuestats.agent

import org.joda.time.DateTime
import xivo.queuestats.model.QueueLog

import scala.collection.mutable.ListBuffer

case class QueueLogInterval(start: DateTime, end: DateTime, qls: ListBuffer[QueueLog])
