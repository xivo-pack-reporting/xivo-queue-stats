package xivo.queuestats.agent

import java.sql.Connection

import org.joda.time.{DateTime, Period}
import xivo.queuestats.BoundedManager
import xivo.queuestats.model.{AgentState, QueueLog, StatAgentPeriodic}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer

class StatAgentManager(interval: Period, _startTime: Option[DateTime], _endTime: Option[DateTime])(implicit conn: Connection)
  extends BoundedManager[QueueLog.type, StatAgentPeriodic.type](interval, QueueLog, StatAgentPeriodic) {

  val batchSize = 50000
  override lazy val startTime = _startTime.getOrElse(super.startTime)
  override lazy val endTime = _endTime.getOrElse(super.endTime)

  def computePeriods(start: DateTime, end:DateTime): List[(DateTime, DateTime)] = {
    var localStart = start
    var nextStart = start.plus(interval)
    val res = ListBuffer[(DateTime, DateTime)]()
    while(!nextStart.isAfter(end)) {
      res.append((localStart, nextStart))
      localStart = nextStart
      nextStart = nextStart.plus(interval)
    }
    res.toList
  }

  def insertNewStatAgentPeriodic() {
    val periods = computePeriods(startTime, endTime)

    val agentStates = mutable.Map(AgentState.getStates().toSeq: _*)
    val buffer = new StatAgentPeriodicBuffer(batchSize, StatAgentPeriodic.insertAll(_))
    for(qlInt <- new QueueLogIntervalBuffer(periods)) {
      val processor = new AgentStateProcessor(qlInt.start, qlInt.end, agentStates)
      buffer.appendAll(processor.processQueueLogs(qlInt.qls))
    }
    buffer.flush()
    AgentState.deleteAll()
    AgentState.insertStates(agentStates.toMap)
  }
}
