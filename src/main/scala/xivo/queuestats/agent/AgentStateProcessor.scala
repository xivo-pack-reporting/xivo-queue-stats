package xivo.queuestats.agent

import org.joda.time.{Period, DateTime}
import xivo.queuestats.model.{AgentState, StatAgentPeriodic, QueueLog, State}
import scala.collection.mutable.{Map => MutableMap}

class AgentStateProcessor(start: DateTime, end: DateTime, agentStates: MutableMap[String, AgentState]) {

  val agentPattern = "Agent/(\\d+)".r
  private val res = MutableMap[String, StatAgentPeriodic]()

  def extractAgentNumber(s: String): String = {
    agentPattern.findAllIn(s).matchData foreach {
      m => return m.group(1)
    }
    s
  }

  def processQueueLogs(logs: Iterable[QueueLog]): List[StatAgentPeriodic] = {
    initialiseStats()
    for(ql <- logs) {
      val agentNum = extractAgentNumber(ql.agent)
      if(!agentStates.contains(agentNum)) {
        processNewAgent(agentNum)
      }
      val periodStart = max(start, agentStates(agentNum).date)
      val elapsedSeconds = new Period(periodStart, new DateTime(ql.time)).toStandardSeconds.getSeconds
      ql.event match {
        case "AGENTCALLBACKLOGIN" => agentStates.put(agentNum, AgentState(new DateTime(ql.time), State.LoggedOn))
        case "AGENTCALLBACKLOGOFF" => {
          res(agentNum).loginTime += elapsedSeconds
          if(agentStates(agentNum).state == State.Paused)
            res(agentNum).pauseTime += elapsedSeconds
          agentStates.put(agentNum, AgentState(new DateTime(ql.time), State.LoggedOff))
        }
        case "PAUSEALL" => {
          res(agentNum).loginTime += elapsedSeconds
          agentStates.put(agentNum, AgentState(new DateTime(ql.time), State.Paused))
        }
        case "UNPAUSEALL" => {
          res(agentNum).loginTime += elapsedSeconds
          res(agentNum).pauseTime += elapsedSeconds
          agentStates.put(agentNum, AgentState(new DateTime(ql.time), State.LoggedOn))
        }
        case "WRAPUPSTART" => res(agentNum).wrapupTime += Integer.parseInt(ql.data1.get)
      }
    }
    finaliseStats()
    res.values.filter(stat => stat.loginTime > 0 || stat.wrapupTime > 0 || stat.pauseTime > 0).toList
  }

  def finaliseStats() {
    for ((agent, state) <- agentStates) {
      val elapsedSeconds = new Period(max(start, state.date), end).toStandardSeconds.getSeconds
      if (state.state == State.LoggedOn)
        res(agent).loginTime += elapsedSeconds
      if (state.state == State.Paused) {
        res(agent).loginTime += elapsedSeconds
        res(agent).pauseTime += elapsedSeconds
      }
    }
  }

  def processNewAgent(agentNum: String) {
    agentStates.put(agentNum, AgentState(new DateTime(start), State.LoggedOn))
    res.put(agentNum, emptyStatAgentPeriodic(agentNum))
  }

  def initialiseStats() {
    for (agent <- agentStates.keys) {
      res.put(agent, emptyStatAgentPeriodic(agent))
    }
  }

  def max(d1: DateTime, d2: DateTime): DateTime = if(d1.isAfter(d2)) d1 else d2

  def emptyStatAgentPeriodic(number: String) = StatAgentPeriodic(None, start.toDate, number, 0, 0, 0)

}