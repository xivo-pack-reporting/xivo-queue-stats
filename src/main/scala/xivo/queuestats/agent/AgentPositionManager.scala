package xivo.queuestats.agent

import java.sql.Connection

import org.joda.time.DateTime
import xivo.queuestats.EmptyTableException
import xivo.queuestats.model.{AgentPosition, QueueLog}

import scala.collection.mutable.{Map => MutableMap}

class AgentPositionManager() (implicit conn: Connection) {

  val startTime = AgentPosition.getMaxDate().getOrElse(QueueLog.getMinDate() match {
    case None => throw new EmptyTableException("queue_log")
    case Some(d) => d.minusSeconds(1)
  })

  def fillAgentPositions() = {
    val qls = QueueLog.loginActionsSince(startTime)
    val positions = new AgentPositionBuffer()
    for(ql <- qls) {
      if(ql.event.equals("AGENTCALLBACKLOGIN")) {
        val lineNum = extractLineNumber(ql.data1.get)
        positions.append(AgentPosition(extractAgent(ql.agent), lineNum, AgentPosition.sdaFromLineNum(lineNum), new DateTime(ql.time), None))
      } else if(ql.event.equals("AGENTCALLBACKLOGOFF")) {
        val agent = extractAgent(ql.agent)
        positions.getLastPosition(agent) match {
          case Some(position) if(position.endTime == None) =>  {
            positions.removeLastPosition(agent)
            positions.append(position.copy(endTime = Some(new DateTime(ql.time))))
          }
          case _ =>
        }
      }
    }
    AgentPosition.removePendingLoginForAgents(positions.allAgents())
    positions.allPositions().foreach(AgentPosition.insert(_))
  }

  def extractAgent(agentStr: String) = agentStr.replace("Agent/", "")

  def extractLineNumber(qualifiedExten: String): String = if(qualifiedExten.contains("@")) qualifiedExten.substring(0, qualifiedExten.indexOf("@")) else qualifiedExten

}

class AgentPositionBuffer()(implicit c: Connection) {
  val map = MutableMap[String, List[AgentPosition]]()

  def append(position: AgentPosition) = map.get(position.agent) match {
    case None => map.put(position.agent, List(position))
    case Some(l) => map.put(position.agent, position::l)
  }

  def removeLastPosition(agent: String) = map.get(agent) match {
    case Some(head::tail) => map.put(agent, tail)
    case _ =>
  }

  def allPositions(): List[AgentPosition] = map.values.flatten.toList

  def getLastPosition(agent: String): Option[AgentPosition] = map.get(agent) match {
    case None => {
      val res = AgentPosition.lastPendingForAgent(agent)
      map.put(agent, List(res).flatten)
      res
    }
    case Some(head::tail) => Some(head)
    case Some(List()) => None
  }

  def allAgents(): List[String] = map.keys.toList
}
