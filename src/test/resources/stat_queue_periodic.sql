DROP TABLE IF EXISTS "stat_queue_periodic" CASCADE;
CREATE TABLE "stat_queue_periodic" (
 "id" SERIAL PRIMARY KEY,
 "time" timestamp NOT NULL,
 "queue" VARCHAR(128) NOT NULL,
 "answered" INTEGER NOT NULL DEFAULT 0,
 "abandoned" INTEGER NOT NULL DEFAULT 0,
 "total" INTEGER NOT NULL DEFAULT 0,
 "full" INTEGER NOT NULL DEFAULT 0,
 "closed" INTEGER NOT NULL DEFAULT 0,
 "joinempty" INTEGER NOT NULL DEFAULT 0,
 "leaveempty" INTEGER NOT NULL DEFAULT 0,
 "divert_ca_ratio" INTEGER NOT NULL DEFAULT 0,
 "divert_waittime" INTEGER NOT NULL DEFAULT 0,
 "timeout" INTEGER NOT NULL DEFAULT 0
);

DROP FUNCTION IF EXISTS round_to_x_seconds(timestamp without time zone, integer);
CREATE OR REPLACE FUNCTION round_to_x_seconds(the_date timestamp without time zone, seconds integer) RETURNS timestamp without time zone AS
$$
DECLARE
  delta INTERVAL;
BEGIN
  delta := the_date - the_date::timestamp with time zone at time zone 'UTC';
  RETURN the_date - (((CAST(extract(epoch FROM the_date + delta) AS INTEGER) + 3*24*3600) % seconds) * INTERVAL '1 second');
END
$$
LANGUAGE plpgsql;
