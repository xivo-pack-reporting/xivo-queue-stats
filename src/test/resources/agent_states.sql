DROP TYPE IF EXISTS agent_state_type CASCADE;
CREATE TYPE agent_state_type AS ENUM('logged_on', 'logged_off', 'paused');

DROP TABLE IF EXISTS agent_states;
CREATE TABLE agent_states (
    agent VARCHAR(128),
    time TIMESTAMP WITHOUT TIME ZONE,
    state agent_state_type
);
