DROP TABLE IF EXISTS dialaction;

CREATE TABLE "dialaction" (
 "event" VARCHAR(80) NOT NULL,
 "category" VARCHAR(80),
 "categoryval" VARCHAR(128) NOT NULL DEFAULT '',
 "action" VARCHAR(80) NOT NULL,
 "actionarg1" VARCHAR(255) DEFAULT NULL::character varying,
 "actionarg2" VARCHAR(255) DEFAULT NULL::character varying,
 "linked" INTEGER NOT NULL DEFAULT 0,
 PRIMARY KEY("event","category","categoryval")
);