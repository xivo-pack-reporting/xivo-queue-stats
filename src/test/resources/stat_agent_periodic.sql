DROP TABLE IF EXISTS "stat_agent_periodic" CASCADE;
CREATE TABLE "stat_agent_periodic" (
 "id" SERIAL PRIMARY KEY,
 "time" timestamp NOT NULL,
 "agent" VARCHAR(128),
 "login_time" INTERVAL NOT NULL DEFAULT '00:00:00',
 "pause_time" INTERVAL NOT NULL DEFAULT '00:00:00',
 "wrapup_time" INTERVAL NOT NULL DEFAULT '00:00:00'
);
