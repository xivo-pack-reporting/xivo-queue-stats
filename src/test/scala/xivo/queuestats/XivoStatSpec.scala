package xivo.queuestats

import java.sql.Connection

import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.dbunit.DBUtil
import xivo.queuestats.model._

class XivoStatSpec extends FlatSpec with Matchers with BeforeAndAfterEach with BeforeAndAfterAll {

  var factory: ConnectionFactory = null
  implicit var connection: Connection = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val tablesToClean = List("queue_log", "last_queue_log_id", "call_on_queue")
  val period = new Period(0, 15, 0, 0)

  override def beforeAll() {
    DBUtil.setupDB("queuestats.xml")
    factory = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD)
    connection = factory.getConnection
  }

  override def beforeEach() {
    tablesToClean.foreach(t => DBUtil.cleanTable(t))
  }

  "XivoStat" should "generate call_on_queue" in {
    CallOnQueueTestUtils.insertCel("13455.789", "13455.789")
    val ql1 = QueueLog(40, format.parseDateTime("2013-01-01 07:59:00").toDate, "13455.789", "queue1", "NONE",
      "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(41, format.parseDateTime("2013-01-01 08:00:20").toDate, "13455.789", "queue1", "NONE",
      "ABANDON", None, None, Some("30"), None, None)
    QueueLogTestUtils.insertQueueLog(ql1)
    QueueLogTestUtils.insertQueueLog(ql2)

    new XivoStat(connection, period, None, None).generateCallOnQueues

    val expectedCall = CallOnQueue(None, "13455.789", format.parseDateTime("2013-01-01 07:59:00").toDate, 0, None,
      Some(format.parseDateTime("2013-01-01 08:00:20").toDate), Some(CallExitType.Abandoned), "queue1", None)
    CallOnQueueTestUtils.allCallOnQueues() should equal(List(expectedCall))
  }

  it should "not update newly inserted stat_queue_periodic" in {
    val callTimeout = CallOnQueue(None, "123456.820", format.parseDateTime("2014-01-01 08:14:59").toDate, 5,
      None, Some(format.parseDateTime("2014-01-01 08:17:59").toDate),
      Some(CallExitType.Timeout), "queue01", Some("2000"))
    CallOnQueue.insert(List(callTimeout))

    new XivoStat(connection, period, Some(format.parseDateTime("2014-01-01 08:00:00")),
      Some(format.parseDateTime("2014-01-01 08:30:00"))).generateStatQueuePeriodic(List(callTimeout))

    StatQueuePeriodicTestUtils.allStatQueuePeriodic() shouldEqual List(
      StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00").toDate, "queue01", 0, 0, 1, 0, 0, 0, 0, 0, 0, 1))
  }
}
