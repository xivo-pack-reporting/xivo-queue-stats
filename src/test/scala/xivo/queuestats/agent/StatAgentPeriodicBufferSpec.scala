package xivo.queuestats.agent

import org.joda.time.format.DateTimeFormat
import org.scalatest.mock.EasyMockSugar
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.model.StatAgentPeriodic

import scala.collection.mutable.ListBuffer

class StatAgentPeriodicBufferSpec extends FlatSpec with Matchers with BeforeAndAfterEach with EasyMockSugar {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val stat1 = StatAgentPeriodic(None, format.parseDateTime("2012-01-01 08:00:00").toDate, "2000", 30, 15, 10)
  val stat2 = StatAgentPeriodic(None, format.parseDateTime("2012-01-01 08:15:00").toDate, "2000", 30, 15, 10)
  val stat3 = StatAgentPeriodic(None, format.parseDateTime("2012-01-01 08:30:00").toDate, "2000", 30, 15, 10)
  val stat4 = StatAgentPeriodic(None, format.parseDateTime("2012-01-01 08:45:00").toDate, "2000", 30, 15, 10)
  var insertMethod: Iterable[StatAgentPeriodic] => Unit = null

  override def beforeEach() {
    insertMethod = mock[Iterable[StatAgentPeriodic] => Unit]
  }

  "An AgentStateBuffer" should "accumulate agent states" in {
    val buff = new StatAgentPeriodicBuffer(20, insertMethod)
    val states1 = List(stat1, stat2)
    val states2 = List(stat3, stat4)

    buff.appendAll(states1)
    buff.appendAll(states2)

    buff.stats shouldEqual ListBuffer(stat1, stat2, stat3, stat4)
  }

  it should "insert the data and clear the list when the batch size is reached" in {
    val buff = new StatAgentPeriodicBuffer(3, insertMethod)
    val states1 = List(stat1, stat2)
    val states2 = List(stat3, stat4)
    call(insertMethod.apply(ListBuffer(stat1, stat2, stat3, stat4))).andReturn()

    whenExecuting(insertMethod) {
      buff.appendAll(states1)
      buff.appendAll(states2)
    }

    buff.stats shouldEqual ListBuffer()
  }

  it should "flush" in {
    val buff = new StatAgentPeriodicBuffer(3, insertMethod)
    val states1 = List(stat1, stat2)
    buff.appendAll(states1)

    call(insertMethod.apply(ListBuffer(stat1, stat2))).andReturn()
    whenExecuting(insertMethod) {
      buff.flush()
    }

    buff.stats shouldEqual ListBuffer()
  }
}
