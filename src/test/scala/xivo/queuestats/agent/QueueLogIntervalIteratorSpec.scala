package xivo.queuestats.agent

import java.security.InvalidParameterException
import java.util.Date

import org.easymock.EasyMock
import org.joda.time.format.DateTimeFormat
import org.scalatest.mock.EasyMockSugar
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.model.QueueLog

import scala.collection.mutable.ListBuffer

class QueueLogIntervalIteratorSpec extends FlatSpec with Matchers with BeforeAndAfterEach with EasyMockSugar {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  var fetchMethod: (Date, Date) => List[QueueLog] = null
  val d1 = format.parseDateTime("2012-01-01 08:00:00")
  val d2 = format.parseDateTime("2012-01-01 08:15:00")
  val d3 = format.parseDateTime("2012-01-01 08:30:00")
  val d4 = format.parseDateTime("2012-01-01 08:45:00")
  val ql1 = QueueLog(0, d1.plusMinutes(1).toDate, "123456.789", "queue", "Agent/1000", "AGENTCALLBACKLOGIN", None, None, None, None, None)
  val ql2 = QueueLog(0, d1.plusMinutes(2).toDate, "123456.790", "queue", "Agent/1000", "PAUSEALL", None, None, None, None, None)
  val ql3 = QueueLog(0, d2.plusMinutes(1).toDate, "123456.791", "queue", "Agent/1000", "AGENTCALLBACKLOGOFF", None, None, None, None, None)
  val ql4 = QueueLog(0, d2.plusMinutes(2).toDate, "123456.792", "queue", "Agent/1000", "PAUSEALL", None, None, None, None, None)
  val ql5 = QueueLog(0, d3.plusMinutes(1).toDate, "123456.793", "queue", "Agent/1000", "AGENTCALLBACKLOGOFF", None, None, None, None, None)
  val ql6 = QueueLog(0, d3.plusMinutes(2).toDate, "123456.794", "queue", "Agent/1000", "PAUSEALL", None, None, None, None, None)
  val ql7 = QueueLog(0, d3.plusMinutes(3).toDate, "123456.795", "queue", "Agent/1000", "AGENTCALLBACKLOGOFF", None, None, None, None, None)

  override def beforeEach() {
    fetchMethod = mock[(Date, Date) => List[QueueLog]]
  }

  "A QueueLogIntervalIterator" should "return false if there are no intervals" in {
    val it = new QueueLogIntervalIterator(List(), 10, fetchMethod)
    it.hasNext shouldBe false
  }

  it should "refuse to be created with a non-strictly-positive batch size" in {
    an[InvalidParameterException] should be thrownBy new QueueLogIntervalIterator(List(), -2, fetchMethod)
    an[InvalidParameterException] should be thrownBy new QueueLogIntervalIterator(List(), 0, fetchMethod)
  }

  it should "return true while an interval is remaining" in {
    fetchMethod.apply(EasyMock.anyObject(), EasyMock.anyObject()).andReturn(List(ql1, ql2, ql3, ql4, ql5, ql6, ql7))
    whenExecuting(fetchMethod) {
      val it = new QueueLogIntervalIterator(List((d1, d2), (d2, d3), (d3, d4)), 10, fetchMethod)
      it.hasNext shouldBe true
      it.next
      it.hasNext shouldBe true
      it.next
      it.hasNext shouldBe true
      it.next
      it.hasNext shouldBe false
    }
  }

  it should "return the next interval with its associated queue logs" in {
    fetchMethod.apply(d1.toDate, d2.toDate).andReturn(List(ql1, ql2))

    whenExecuting(fetchMethod) {
      val it = new QueueLogIntervalIterator(List((d1, d2)), 10, fetchMethod)
      it.next() shouldEqual QueueLogInterval(d1, d2, ListBuffer(ql1, ql2))
    }
  }

  it should "throw an exception if next is called on an empty list" in {
    val it = new QueueLogIntervalIterator(List(), 10, fetchMethod)
    an[NoSuchElementException] should be thrownBy(it.next())
  }

  it should "limit calls to the fetch method with the batch size" in {
    fetchMethod.apply(d1.toDate, d3.toDate).andReturn(List(ql1, ql2, ql3, ql4))
    fetchMethod.apply(d3.toDate, d4.toDate).andReturn(List(ql5, ql6, ql7))
    whenExecuting(fetchMethod) {
      val it = new QueueLogIntervalIterator(List((d1, d2), (d2, d3), (d3, d4)), 2, fetchMethod)
      it.hasNext shouldBe true
      it.next shouldEqual QueueLogInterval(d1, d2, ListBuffer(ql1, ql2))
      it.hasNext shouldBe true
      it.next shouldEqual QueueLogInterval(d2, d3, ListBuffer(ql3, ql4))
      it.hasNext shouldBe true
      it.next shouldEqual QueueLogInterval(d3, d4, ListBuffer(ql5, ql6, ql7))
      it.hasNext shouldBe false
    }
  }

  it should "deal properly with empty intervals" in {
    fetchMethod.apply(EasyMock.anyObject(), EasyMock.anyObject()).andReturn(List(ql1, ql2, ql5, ql6, ql7))
    whenExecuting(fetchMethod) {
      val it = new QueueLogIntervalIterator(List((d1, d2), (d2, d3), (d3, d4)), 10, fetchMethod)
      it.hasNext shouldBe true
      it.next shouldEqual QueueLogInterval(d1, d2, ListBuffer(ql1, ql2))
      it.hasNext shouldBe true
      it.next shouldEqual QueueLogInterval(d2, d3, ListBuffer())
      it.hasNext shouldBe true
      it.next shouldEqual QueueLogInterval(d3, d4, ListBuffer(ql5, ql6, ql7))
      it.hasNext shouldBe false
    }
  }
}
