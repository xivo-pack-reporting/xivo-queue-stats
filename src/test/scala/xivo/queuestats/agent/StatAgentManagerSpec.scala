package xivo.queuestats.agent

import java.sql.Connection

import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.{EmptyTableException, ConnectionFactory}
import xivo.queuestats.dbunit.DBUtil
import xivo.queuestats.model._

class StatAgentManagerSpec extends FlatSpec with Matchers with BeforeAndAfterEach with BeforeAndAfterAll {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  // intervalle de 15 minutes (heures, minutes, secondes, millisecondes)
  val interval = new Period(0, 15, 0, 0)
  var manager: StatAgentManager = null
  implicit var connection: Connection = null

  override def beforeAll() {
    connection = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD).getConnection()
  }
  override def beforeEach() {
    manager = new StatAgentManager(interval, None, None)
    List("queue_log", "stat_agent_periodic", "agent_states").foreach(t => DBUtil.cleanTable(t))
  }

  it should "extract periods from the start and end date, according to the interval" in {
    manager.computePeriods(format.parseDateTime("2013-01-01 08:00:00"), format.parseDateTime("2013-01-01 08:45:00")) shouldEqual List(
    (format.parseDateTime("2013-01-01 08:00:00"), format.parseDateTime("2013-01-01 08:15:00")),
    (format.parseDateTime("2013-01-01 08:15:00"), format.parseDateTime("2013-01-01 08:30:00")),
    (format.parseDateTime("2013-01-01 08:30:00"), format.parseDateTime("2013-01-01 08:45:00")))
  }

  it should "take the start date from stat_agent_periodic and generate stat_agent_periodic" in {
    StatAgentPeriodic.insertAll(List(StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "2000", 900, 0, 0)))
    AgentState.insertStates(Map("2000" -> AgentState(format.parseDateTime("2013-01-01 07:00:00"), State.LoggedOn)))
    QueueLogTestUtils.insertQueueLog(QueueLog(1, format.parseDateTime("2013-01-01 08:21:00").toDate, "123456.789", "NONE",
    "Agent/2000", "PAUSEALL", None, None, None, None, None))
    QueueLogTestUtils.insertQueueLog(QueueLog(2, format.parseDateTime("2013-01-01 08:32:00").toDate, "123456.789", "NONE",
    "Agent/2000", "UNPAUSEALL", None, None, None, None, None))

    manager.startTime shouldEqual format.parseDateTime("2013-01-01 08:15:00")
    manager.endTime shouldEqual format.parseDateTime("2013-01-01 08:30:00")
    manager.insertNewStatAgentPeriodic()

    StatAgentPeriodicTestUtils.allStatAgentPeriodic() shouldEqual List(
        StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "2000", 900, 0, 0),
        StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:15:00").toDate, "2000", 900, 540, 0))
    AgentState.getStates() shouldEqual Map(
        "2000" -> AgentState(format.parseDateTime("2013-01-01 08:21:00"), State.Paused)
    )
  }

  it should "take the start date from queue_log if there are no stat_agent_periodic" in {
    QueueLogTestUtils.insertQueueLog(QueueLog(1, format.parseDateTime("2013-01-01 08:21:00").toDate, "123456.789", "NONE",
      "Agent/2000", "AGENTCALLBACKLOGIN", None, None, None, None, None))
    QueueLogTestUtils.insertQueueLog(QueueLog(2, format.parseDateTime("2013-01-01 08:32:00").toDate, "123456.789", "NONE",
      "Agent/2000", "AGENTCALLBACKLOGOFF", None, None, None, None, None))

    manager.startTime shouldEqual format.parseDateTime("2013-01-01 08:15:00")
    manager.endTime shouldEqual format.parseDateTime("2013-01-01 08:30:00")
  }

  it should "use the provided start and end dates" in {
    val manager = new StatAgentManager(interval, Some(format.parseDateTime("2014-01-01 00:00:00")), Some(format.parseDateTime("2014-01-01 02:00:00")))
    manager.startTime shouldEqual format.parseDateTime("2014-01-01 00:00:00")
    manager.endTime shouldEqual format.parseDateTime("2014-01-01 02:00:00")
  }

  it should "throw an exception if no queue log is found" in {
    intercept[EmptyTableException] {
      manager.insertNewStatAgentPeriodic()
    }
  }
}
