package xivo.queuestats.agent

import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.model.{AgentState, QueueLog, StatAgentPeriodic, State}
import scala.collection.mutable.{Map => MutableMap}

class AgentStateProcessorSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val start = format.parseDateTime("2013-01-01 08:00:00")
  val end = format.parseDateTime("2013-01-01 09:00:00")
  var processor: AgentStateProcessor = null
  var agentStates = MutableMap[String, AgentState]()

  override def beforeEach() {
    agentStates = MutableMap("1000" -> AgentState(format.parseDateTime("2013-01-01 07:00:00"), State.LoggedOff),
      "2000" -> AgentState(format.parseDateTime("2013-01-01 07:00:00"), State.LoggedOn),
      "3000" -> AgentState(format.parseDateTime("2013-01-01 07:00:00"), State.LoggedOn))
    processor = new AgentStateProcessor(start, end, agentStates)
  }

  "An OtherAgentStateProcessor" should "calculate login time and pause time" in {
    val ql0 = QueueLog(40, format.parseDateTime("2013-01-01 08:15:00").toDate, "13455.789", "NONE", "Agent/1000",
      "AGENTCALLBACKLOGIN", Some("2014@default"), None, None, None, None)
    val ql1 = QueueLog(41, format.parseDateTime("2013-01-01 08:20:00").toDate, "13455.789", "NONE", "Agent/2000",
      "PAUSEALL", None, None, None, None, None)
    val ql2 = QueueLog(42, format.parseDateTime("2013-01-01 08:30:00").toDate, "13455.789", "NONE", "Agent/2000",
      "AGENTCALLBACKLOGOFF", Some("2014@default"), None, None, None, None)
    val ql3 = QueueLog(43, format.parseDateTime("2013-01-01 08:32:00").toDate, "13455.789", "NONE", "Agent/1000",
      "PAUSEALL", None, None, None, None, None)
    val ql4 = QueueLog(44, format.parseDateTime("2013-01-01 08:35:00").toDate, "13455.789", "NONE", "Agent/3000",
      "PAUSEALL", None, None, None, None, None)
    val ql5 = QueueLog(45, format.parseDateTime("2013-01-01 08:40:00").toDate, "13455.789", "NONE", "Agent/3000",
      "UNPAUSEALL", None, None, None, None, None)
    val ql6 = QueueLog(45, format.parseDateTime("2013-01-01 08:51:00").toDate, "13455.789", "NONE", "Agent/3000",
      "WRAPUPSTART", Some("30"), None, None, None, None)

    processor.processQueueLogs(List(ql0, ql1, ql2, ql3, ql4, ql5, ql6)).sorted shouldEqual List(
      StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "1000", 2700, 1680, 0),
      StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "2000", 1800, 600, 0),
      StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "3000", 3600, 300, 30)).sorted

    agentStates shouldEqual(MutableMap(
      "1000" -> AgentState(format.parseDateTime("2013-01-01 08:32:00"), State.Paused),
      "2000" -> AgentState(format.parseDateTime("2013-01-01 08:30:00"), State.LoggedOff),
      "3000" -> AgentState(format.parseDateTime("2013-01-01 08:40:00"), State.LoggedOn)))
  }

  it should "not keep StatAgentPeriodic whose agent is always logged off" in {
    processor.processQueueLogs(List()).sorted shouldEqual List(StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "2000", 3600, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "3000", 3600, 0, 0)).sorted
  }

  it should "deal with new agents" in {
    val ql0 = QueueLog(40, format.parseDateTime("2013-01-01 08:15:00").toDate, "13455.789", "NONE", "Agent/1000",
      "AGENTCALLBACKLOGIN", Some("2014@default"), None, None, None, None)
    val ql1 = QueueLog(41, format.parseDateTime("2013-01-01 08:20:00").toDate, "13455.789", "NONE", "Agent/1000",
      "PAUSEALL", None, None, None, None, None)
    val ql2 = QueueLog(42, format.parseDateTime("2013-01-01 08:30:00").toDate, "13455.789", "NONE", "Agent/4000",
      "AGENTCALLBACKLOGOFF", Some("2014@default"), None, None, None, None)

    processor.processQueueLogs(List(ql0, ql1, ql2)).sorted shouldEqual List(
      StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "1000", 2700, 2400, 0),
      StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "2000", 3600, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "3000", 3600, 0, 0),
      StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "4000", 1800, 0, 0)).sorted

    agentStates shouldEqual(MutableMap(
      "1000" -> AgentState(format.parseDateTime("2013-01-01 08:20:00"), State.Paused),
      "2000" -> AgentState(format.parseDateTime("2013-01-01 07:00:00"), State.LoggedOn),
      "3000" -> AgentState(format.parseDateTime("2013-01-01 07:00:00"), State.LoggedOn),
      "4000" -> AgentState(format.parseDateTime("2013-01-01 08:30:00"), State.LoggedOff)))
  }
}
