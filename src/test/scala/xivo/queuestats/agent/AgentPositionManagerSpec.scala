package xivo.queuestats.agent

import java.sql.Connection

import anorm._
import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.ConnectionFactory
import xivo.queuestats.dbunit.DBUtil
import xivo.queuestats.model.{AgentPosition, QueueLog, QueueLogTestUtils}

import scala.util.Random

class AgentPositionManagerSpec extends FlatSpec with Matchers with BeforeAndAfterEach with BeforeAndAfterAll {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val random = new Random()
  implicit var connection: Connection = null

  override def beforeAll() {
    connection = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD).getConnection()
    DBUtil.setupDB("queuestats.xml")
  }
  override def beforeEach() {
    List("queue_log", "agent_position", "extensions", "dialaction").foreach(t => DBUtil.cleanTable(t))
  }

  "An AgentPositionManager" should "calculate agent positons" in {
    val pos1 = AgentPosition("1000", "2000", None, format.parseDateTime("2014-01-01 08:00:00"), Some(format.parseDateTime("2014-01-01 18:00:00")))
    val pos2 = AgentPosition("1001", "2001", None, format.parseDateTime("2014-01-01 08:00:00"), None)
    val oldQl = QueueLog(1, format.parseDateTime("2014-01-01 18:00:00").toDate, "NONE", "", "Agent/1000", "AGENTCALLBACKLOGOFF", None, None, None, None, None)
    val login1000 = QueueLog(2, format.parseDateTime("2014-01-02 08:00:00").toDate, "NONE", "", "Agent/1000", "AGENTCALLBACKLOGIN", Some("2002@default"), None, None, None, None)
    val login1002 = QueueLog(3, format.parseDateTime("2014-01-02 08:00:00").toDate, "NONE", "", "Agent/1002", "AGENTCALLBACKLOGIN", Some("2004@default"), None, None, None, None)
    val logoff1001 = QueueLog(4, format.parseDateTime("2014-01-02 18:00:00").toDate, "NONE", "", "Agent/1001", "AGENTCALLBACKLOGOFF", None, None, None, None, None)

    List(pos1, pos2).foreach(AgentPosition.insert(_))
    List(oldQl, login1000, login1002, logoff1001).foreach(QueueLogTestUtils.insertQueueLog(_))

    val manager = new AgentPositionManager()
    manager.fillAgentPositions()

    allAgentPositions() should contain theSameElementsAs  List(
      pos1,
      AgentPosition("1001", "2001", None, format.parseDateTime("2014-01-01 08:00:00"), Some(format.parseDateTime("2014-01-02 18:00:00"))),
      AgentPosition("1000", "2002", None, format.parseDateTime("2014-01-02 08:00:00"), None),
      AgentPosition("1002", "2004", None, format.parseDateTime("2014-01-02 08:00:00"), None))
  }

  it should "calculate agent positions with the SDA" in {
    createSdaForLine("0213457896", "2000")
    QueueLogTestUtils.insertQueueLog(QueueLog(3, format.parseDateTime("2014-01-02 08:00:00").toDate, "NONE", "", "Agent/1002", "AGENTCALLBACKLOGIN", Some("2000@default"), None, None, None, None))

    new AgentPositionManager().fillAgentPositions()

    allAgentPositions() shouldEqual List(AgentPosition("1002", "2000", Some("0213457896"), format.parseDateTime("2014-01-02 08:00:00"), None))
  }

  it should "retrieve the start date from the agent_position table if it is not empty" in {
    AgentPosition.insert(AgentPosition("1000", "1000", Some("31000"), format.parseDateTime("2014-01-01 08:00:00"), None))

    val manager = new AgentPositionManager()

    manager.startTime shouldEqual format.parseDateTime("2014-01-01 08:00:00")
  }

  it should "retrieve the start date from the queue_log table minus 1 second otherwise" in {
    QueueLogTestUtils.insertQueueLog(QueueLog(1, format.parseDateTime("2014-01-01 08:00:00").toDate, "123456.789", "ha", "1000", "AGENTCALLBACKLOGIN", None, None, None, None, None))

    new AgentPositionManager().startTime shouldEqual format.parseDateTime("2014-01-01 07:59:59")
  }

  private def allAgentPositions(): List[AgentPosition] =
    SQL("SELECT agent_num, line_number, sda, start_time, end_time FROM agent_position").as(AgentPosition.simple *)

  private def createSdaForLine(sda: String, line: String): Unit = {
    val userId = random.nextInt()
    val incallId = random.nextInt()
    SQL("INSERT INTO dialaction(event, category, categoryval, action, actionarg1) VALUES ('answer', 'incall', {incallId}, 'user', {userId})")
      .on('userId -> userId, 'incallId -> incallId).executeUpdate()
    SQL("INSERT INTO extensions(exten, type, typeval) VALUES ({sda}, 'incall', {incallId})").on('incallId -> incallId, 'sda -> sda).executeUpdate()
    SQL("INSERT INTO extensions(exten, type, typeval) VALUES ({line}, 'user', {userId})").on('userId -> userId, 'line -> line).executeUpdate()
  }

}
