package xivo.queuestats.queue

import java.sql.Connection

import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.dbunit.DBUtil
import xivo.queuestats.model._
import xivo.queuestats.{ConnectionFactory, EmptyTableException}

class StatQueueManagerSpec extends FlatSpec with Matchers with BeforeAndAfterEach with BeforeAndAfterAll {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  // intervalle de 15 minutes (heures, minutes, secondes, millisecondes)
  val interval = new Period(0, 15, 0, 0)
  var manager: StatQueueManager = null
  implicit var connection: Connection = null

  override def beforeAll() {
    connection = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD).getConnection()
  }
  override def beforeEach() {
    manager = new StatQueueManager(interval, None, None)
    List("call_on_queue", "stat_queue_periodic").foreach(t => DBUtil.cleanTable(t))
  }

  "A StatQueueManager" should "get the start date from the existing stat_queue_periodic and generate the new stat_queue_periodic" in {
    StatQueuePeriodicTestUtils.insertStatQueuePeriodic(StatQueuePeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "queue01", 2, 0, 2, 0, 0, 0, 0, 0, 0, 0))
    CallOnQueue.insert(List(
      CallOnQueue(None, "123456.789", format.parseDateTime("2013-01-01 08:25:00").toDate, 2, Some(format.parseDateTime("2013-01-01 08:25:05").toDate),
        Some(format.parseDateTime("2013-01-01 08:26:10").toDate), Some(CallExitType.Answered), "queue01", Some("2001")),
      CallOnQueue(None, "123456.799", format.parseDateTime("2013-01-01 08:35:00").toDate, 2, Some(format.parseDateTime("2013-01-01 08:35:05").toDate),
        Some(format.parseDateTime("2013-01-01 08:36:10").toDate), Some(CallExitType.Answered), "queue01", Some("2001"))))

    manager.startTime shouldEqual format.parseDateTime("2013-01-01 08:15:00")
    manager.endTime shouldEqual format.parseDateTime("2013-01-01 08:30:00")
    manager.insertNewStatQueuePeriodic()

    StatQueuePeriodicTestUtils.allStatQueuePeriodic() shouldEqual List(
      StatQueuePeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "queue01", 2, 0, 2, 0, 0, 0, 0, 0, 0, 0),
      StatQueuePeriodic(None, format.parseDateTime("2013-01-01 08:15:00").toDate, "queue01", 1, 0, 1, 0, 0, 0, 0, 0, 0, 0))
  }

  it should "get the start date from the call_on_queue if there is no stat_queue_periodic" in {
    CallOnQueue.insert(List(
      CallOnQueue(None, "123456.789", format.parseDateTime("2013-01-01 08:25:00").toDate, 2, Some(format.parseDateTime("2013-01-01 08:25:05").toDate),
        Some(format.parseDateTime("2013-01-01 08:26:10").toDate), Some(CallExitType.Answered), "queue01", Some("2001")),
      CallOnQueue(None, "123456.799", format.parseDateTime("2013-01-01 08:35:00").toDate, 2, Some(format.parseDateTime("2013-01-01 08:35:05").toDate),
        Some(format.parseDateTime("2013-01-01 08:36:10").toDate), Some(CallExitType.Answered), "queue01", Some("2001"))))

    manager.startTime shouldEqual format.parseDateTime("2013-01-01 08:15:00")
    manager.endTime shouldEqual format.parseDateTime("2013-01-01 08:30:00")
  }

  it should "throw an exception if there is no call_on_queue" in {
    intercept[EmptyTableException] {
      manager.insertNewStatQueuePeriodic()
    }
  }

  it should "use the provided start and end dates" in {
    val manager = new StatQueueManager(interval, Some(format.parseDateTime("2014-01-01 00:00:00")), Some(format.parseDateTime("2014-01-01 02:00:00")))
    manager.startTime shouldEqual format.parseDateTime("2014-01-01 00:00:00")
    manager.endTime shouldEqual format.parseDateTime("2014-01-01 02:00:00")
  }

  it should "update former StatQueuePeriodic" in {
    val oldCall = CallOnQueue(None, "123456.789", format.parseDateTime("2014-01-01 08:03:00").toDate, 0, None, None, None, "queue01", None)
    val newCall = CallOnQueue(None, "123456.789", format.parseDateTime("2014-01-01 08:03:00").toDate, 0, None,
      Some(format.parseDateTime("2014-01-01 08:03:20").toDate), Some(CallExitType.Abandoned), "queue01", None)
    CallOnQueue.insert(List(newCall))
    val qp = StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00").toDate, "queue01", 3, 2, 8, 1, 0, 0, 0, 0, 0, 1)
    StatQueuePeriodicTestUtils.insertStatQueuePeriodic(qp)

    manager.updateFormerStatQueuePeriodic(List(oldCall))

    StatQueuePeriodicTestUtils.allStatQueuePeriodic() shouldEqual
      List(StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00").toDate, "queue01", 3, 3, 8, 1, 0, 0, 0, 0, 0, 1))
  }
}
