package xivo.queuestats.model

import java.sql.Connection
import java.util.Date

import anorm.SqlParser._
import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.ConnectionFactory
import xivo.queuestats.dbunit.DBUtil
import anorm.{~, SQL}

class StatAgentPeriodicSpec extends FlatSpec with Matchers with BeforeAndAfterEach with BeforeAndAfterAll {

  var factory: ConnectionFactory = null
  implicit var connection: Connection = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")

  override def beforeAll() {
    DBUtil.setupDB("queuestats.xml")
    factory = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD)
    connection = factory.getConnection
  }

  override def beforeEach() {
    DBUtil.cleanTable("stat_agent_periodic")
  }

  "The StatAgentPeriodic singleton" should "insert several StatAgentPeriodic" in {
    val stat1 = StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "2000", 3600, 600, 100)
    val stat2 = StatAgentPeriodic(None, format.parseDateTime("2013-01-01 09:00:00").toDate, "2000", 3600, 600, 100)
    val stat3 = StatAgentPeriodic(None, format.parseDateTime("2013-01-01 10:00:00").toDate, "2000", 3600, 600, 100)
    StatAgentPeriodic.insertAll(List(stat1, stat2, stat3))

    StatAgentPeriodicTestUtils.allStatAgentPeriodic() shouldEqual List(stat1, stat2, stat3)
  }

  def insertStatAgentPeriodic(stat: StatAgentPeriodic) {
    SQL("""INSERT INTO stat_agent_periodic(time, agent, login_time, pause_time, wrapup_time) VALUES
      ({time}, {agent}, {loginTime} * INTERVAL '1 second', {pauseTime} * INTERVAL '1 second', {wrapupTime} * INTERVAL '1 second')""")
      .on('time -> stat.time, 'agent -> stat.agent, 'loginTime -> stat.loginTime, 'pauseTime -> stat.pauseTime, 'wrapupTime -> stat.wrapupTime)
    .executeUpdate()
  }

}

object StatAgentPeriodicTestUtils {

  val simple = get[Date]("time") ~
    get[String]("agent") ~
    get[Double]("login_time") ~
    get[Double]("pause_time") ~
    get[Double]("wrapup_time")  map {
    case time ~ agent~ login~ pause ~ wrapup =>  StatAgentPeriodic(None, time, agent, login.toInt, pause.toInt, wrapup.toInt)
  }

  def allStatAgentPeriodic()(implicit conn: Connection): List[StatAgentPeriodic] =
    SQL("""SELECT time, agent, extract(epoch from login_time) AS login_time,
                             extract(epoch from pause_time) AS pause_time,
                             extract(epoch from wrapup_time) AS wrapup_time FROM stat_agent_periodic""")
      .as(simple *)
}