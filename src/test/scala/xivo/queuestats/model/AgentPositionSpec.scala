package xivo.queuestats.model

import java.sql.Connection

import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.ConnectionFactory
import xivo.queuestats.dbunit.DBUtil

class AgentPositionSpec extends FlatSpec with Matchers with BeforeAndAfterEach with BeforeAndAfterAll {
  var factory: ConnectionFactory = null
  implicit var connection: Connection = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")

  override def beforeAll() {
    DBUtil.setupDB("queuestats.xml")
    factory = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD)
    connection = factory.getConnection
  }

  override def beforeEach() {
    DBUtil.cleanTable("agent_position")
  }

  "The AgentPosition Singleton" should "return the maximum start or end date" in {
    AgentPosition.insert(AgentPosition("1000", "1000", Some("31000"), format.parseDateTime("2014-01-01 08:00:00"), Some(format.parseDateTime("2014-01-01 09:00:00"))))
    AgentPosition.insert(AgentPosition("1100", "1100", Some("31100"), format.parseDateTime("2014-01-01 09:15:00"), None))

    AgentPosition.getMaxDate() shouldEqual Some(format.parseDateTime("2014-01-01 09:15:00"))
  }

  it should "return None if there is no data" in {
    AgentPosition.getMaxDate() shouldEqual None
  }

  it should "return the current agent position if it exists" in {
    AgentPosition.insert(AgentPosition("1100", "1100", Some("31100"), format.parseDateTime("2014-01-01 09:15:00"), None))

    AgentPosition.lastPendingForAgent("1100") shouldEqual Some(AgentPosition("1100", "1100", Some("31100"), format.parseDateTime("2014-01-01 09:15:00"), None))
    AgentPosition.lastPendingForAgent("1200") shouldEqual None
  }
}
