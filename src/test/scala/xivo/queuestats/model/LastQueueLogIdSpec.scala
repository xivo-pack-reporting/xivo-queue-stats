package xivo.queuestats.model

import java.sql.Connection

import anorm._
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.ConnectionFactory
import xivo.queuestats.dbunit.DBUtil

class TestLastCelId extends FlatSpec with Matchers with BeforeAndAfterAll with BeforeAndAfterEach {

  var factory: ConnectionFactory = null
  implicit var connection: Connection = null

  override def beforeAll() = {
    DBUtil.setupDB("queuestats.xml")
    factory = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD)
    connection = factory.getConnection
  }

  override def beforeEach() = {
    DBUtil.cleanTable("last_queue_log_id")
  }

  override def afterAll() = connection.close()

  "The LastCelId singleton" should "insert or create the last queue log id row" in {
    LastQueueLogId.setId(123);

    var rs = SQL("SELECT id FROM last_queue_log_id").resultSet
    rs.next() shouldBe true
    rs.getInt("id") should equal(123)
    rs.next() shouldBe false
    rs.close

    LastQueueLogId.setId(456)
    rs = SQL("SELECT id FROM last_queue_log_id").resultSet
    rs.next() shouldBe true
    rs.getInt("id") should equal(456)
    rs.next() shouldBe false
  }

  it should "retrieve the last queue log id row" in {
    SQL("INSERT INTO last_queue_log_id(id) VALUES (123)").executeUpdate

    LastQueueLogId.getId should equal(123)
  }

  it should "return 0 if no id is found" in {
    LastQueueLogId.getId should equal(0)
  }

}