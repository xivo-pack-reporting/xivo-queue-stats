package xivo.queuestats.model

import java.sql.Connection

import anorm.SQL
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.ConnectionFactory
import xivo.queuestats.dbunit.DBUtil

import scala.collection.mutable

class AgentStateSpec extends FlatSpec with Matchers with BeforeAndAfterEach with BeforeAndAfterAll {

  var factory: ConnectionFactory = null
  implicit var connection: Connection = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")

  override def beforeAll() {
    DBUtil.setupDB("queuestats.xml")
    factory = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD)
    connection = factory.getConnection
  }

  override def beforeEach() {
    DBUtil.cleanTable("agent_states")
  }

  "The AgentState singleton" should "insert the states" in {
    val state1 = AgentState(format.parseDateTime("2013-01-01 08:00:03"), State.LoggedOn)
    val state2 = AgentState(format.parseDateTime("2013-01-01 09:10:03"), State.Paused)

    AgentState.insertStates(Map("1000" -> state1, "2000" -> state2))

    getStates shouldEqual Map("1000" -> state1, "2000" -> state2)
  }

  it should "retrieve the states" in {
    val state1 = AgentState(format.parseDateTime("2013-01-01 08:00:03"), State.LoggedOn)
    val state2 = AgentState(format.parseDateTime("2013-01-01 09:10:03"), State.Paused)

    AgentState.insertStates(Map("1000" -> state1, "2000" -> state2))

    AgentState.getStates shouldEqual Map("1000" -> state1, "2000" -> state2)
  }

  it should "delete all the states" in {
    val state1 = AgentState(format.parseDateTime("2013-01-01 08:00:03"), State.LoggedOn)
    val state2 = AgentState(format.parseDateTime("2013-01-01 09:10:03"), State.Paused)
    AgentState.insertStates(Map("1000" -> state1, "2000" -> state2))

    AgentState.deleteAll()

    AgentState.getStates shouldEqual Map()
  }

  private def getStates(): Map[String, AgentState] = {
    val rs = SQL("SELECT agent, time, state FROM agent_states").resultSet()
    val result = mutable.Map[String, AgentState]()
    while(rs.next())
      result.put(rs.getString("agent"), AgentState(new DateTime(rs.getTimestamp("time")), State.withName(rs.getString("state"))))
    rs.close
    result.toMap
  }
}

