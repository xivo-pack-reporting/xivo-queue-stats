package xivo.queuestats.model

import java.sql.Connection

import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.ConnectionFactory
import xivo.queuestats.dbunit.DBUtil
import anorm.SQL

class QueueLogSpec extends FlatSpec with Matchers with BeforeAndAfterEach with BeforeAndAfterAll {

  var factory: ConnectionFactory = null
  implicit var connection: Connection = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")

  override def beforeAll() {
    DBUtil.setupDB("queuestats.xml")
    factory = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD)
    connection = factory.getConnection
  }

  override def beforeEach() {
    DBUtil.cleanTable("queue_log")
  }

  "The QueueLog singleton" should "retrieve call events by ascending id, starting with the given id plus queue logs linked to a pending call" in {
    val ql0 = QueueLog(40, format.parseDateTime("2013-01-01 07:59:00").toDate, "13455.789", "queue1", "NONE",
      "ENTERQUEUE", None, None, None, None, None)
    val ql1 = QueueLog(41, format.parseDateTime("2013-01-01 08:00:00").toDate, "13456.789", "queue1", "NONE",
      "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(42, format.parseDateTime("2013-01-01 08:10:00").toDate, "13456.789", "queue1", "agent1",
      "CONNECT", None, None, None, None, None)
    val ql3 = QueueLog(43, format.parseDateTime("2013-01-01 08:20:00").toDate, "13456.790", "queue2", "NONE",
      "ENTERQUEUE", None, None, None, None, None)
    val ql4 = QueueLog(44, format.parseDateTime("2013-01-01 08:30:00").toDate, "13456.790", "queue2", "agent1",
      "CONNECT", None, None, None, None, None)
    val ql5 = QueueLog(45, format.parseDateTime("2013-01-01 08:32:00").toDate, "13455.789", "queue1", "NONE",
      "ABANDON", None, None, None, None, None)
    List(ql0, ql3, ql2, ql4, ql1, ql5).foreach(ql => QueueLogTestUtils.insertQueueLog(ql))

    QueueLog.callEventsByAscendingId(42, List("13455.789")) shouldEqual List(ql0, ql2, ql3, ql4, ql5)
  }

  it should "retrieve only call-related events" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00").toDate, "13456.789", "queue1", "NONE",
      "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:10:00").toDate, "13456.789", "queue1", "agent1",
      "CONNECT", None, None, None, None, None)
    val ql3 = QueueLog(3, format.parseDateTime("2013-01-01 08:20:00").toDate, "13456.790", "queue2", "NONE",
      "ENTERQUEUE", None, None, None, None, None)
    val ql4 = QueueLog(4, format.parseDateTime("2013-01-01 08:30:00").toDate, "13456.790", "queue2", "agent1",
      "AGENTCALLBACKLOGIN", None, None, None, None, None)
    List(ql1, ql2, ql3, ql4).foreach(ql => QueueLogTestUtils.insertQueueLog(ql))

    QueueLog.callEventsByAscendingId(1, List()) shouldEqual List(ql1, ql2, ql3)
  }

  it should "retrieve the maximum queue_log id" in {
    for(i <- 1 to 5) {
      QueueLogTestUtils.insertQueueLog(QueueLog(i, format.parseDateTime("2013-01-01 08:00:00").toDate, "13456.789", "queue1", "NONE",
        "ENTERQUEUE", None, None, None, None, None))
    }
    QueueLogTestUtils.insertQueueLog(QueueLog(6, format.parseDateTime("2013-01-01 08:00:00").toDate, "13456.789", "queue1", "NONE",
      "PRESENCE", None, None, None, None, None))
    QueueLog.getMaxQueueLogCallEventsId() shouldEqual Some(5)
  }

  it should "return None if there is no queue_log" in {
    QueueLog.getMaxQueueLogCallEventsId() shouldBe None
  }

  it should "retrieve agent activity events by ascending id, between the two given dates" in {
    val ql0 = QueueLog(40, format.parseDateTime("2013-01-01 07:59:00").toDate, "13455.789", "NONE", "Agent/2000",
      "AGENTCALLBACKLOGIN", None, None, None, None, None)
    val ql1 = QueueLog(41, format.parseDateTime("2013-01-01 08:00:00").toDate, "13455.789", "NONE", "Agent/2001",
      "AGENTCALLBACKLOGIN", None, None, None, None, None)
    val ql2 = QueueLog(42, format.parseDateTime("2013-01-01 08:05:00").toDate, "13455.789", "queue01", "Agent/2001",
      "WRAPUPSTART", None, None, None, None, None)
    val ql3 = QueueLog(43, format.parseDateTime("2013-01-01 08:06:00").toDate, "13455.789", "NONE", "Agent/2001",
      "PAUSEALL", None, None, None, None, None)
    val ql4 = QueueLog(44, format.parseDateTime("2013-01-01 09:07:00").toDate, "13455.789", "NONE", "Agent/2001",
      "UNPAUSEALL", None, None, None, None, None)
    val ql5 = QueueLog(45, format.parseDateTime("2013-01-01 09:10:00").toDate, "13455.789", "NONE", "Agent/2001",
      "ENTERQUEUE", None, None, None, None, None)
    val ql6 = QueueLog(46, format.parseDateTime("2013-01-01 10:01:00").toDate, "13455.789", "NONE", "Agent/2001",
      "AGENTCALLBACKLOGOFF", None, None, None, None, None)
    List(ql0, ql1, ql2, ql3, ql4, ql5, ql6).foreach(ql => QueueLogTestUtils.insertQueueLog(ql))

    QueueLog.agentEventsByAscendingId(format.parseDateTime("2013-01-01 08:00:00").toDate,
      format.parseDateTime("2013-01-01 10:00:00").toDate) shouldEqual List(ql1, ql2, ql3, ql4)
  }

  it should "retrieve agent login events by ascending id, after a given date" in {
    val ql0 = QueueLog(40, format.parseDateTime("2013-01-01 07:59:00").toDate, "13455.789", "NONE", "Agent/2000",
      "AGENTCALLBACKLOGIN", None, None, None, None, None)
    val ql1 = QueueLog(41, format.parseDateTime("2013-01-01 08:00:00").toDate, "13455.789", "NONE", "Agent/2001",
      "AGENTCALLBACKLOGIN", None, None, None, None, None)
    val ql2 = QueueLog(42, format.parseDateTime("2013-01-01 08:05:00").toDate, "13455.789", "queue01", "Agent/2001",
      "WRAPUPSTART", None, None, None, None, None)
    val ql3 = QueueLog(46, format.parseDateTime("2013-01-01 10:01:00").toDate, "13455.789", "NONE", "Agent/2001",
      "AGENTCALLBACKLOGOFF", None, None, None, None, None)
    List(ql0, ql1, ql2, ql3).foreach(ql => QueueLogTestUtils.insertQueueLog(ql))

    QueueLog.loginActionsSince(format.parseDateTime("2013-01-01 07:59:00")) shouldEqual List(ql1, ql3)
  }
}

object QueueLogTestUtils {
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  def insertQueueLog(ql: QueueLog)(implicit conn: Connection) {
    SQL("""INSERT INTO queue_log(id, time, callid, queuename, agent, event, data1, data2, data3, data4, data5)
              VALUES ({id}, {time}, {callid}, {queuename}, {agent}, {event}, {data1}, {data2}, {data3}, {data4}, {data5})""").on(
        'id -> ql.id, 'time -> new DateTime(ql.time).toString(format), 'callid -> ql.callid, 'queuename -> ql.queueName, 'agent -> ql.agent, 'event -> ql.event,
        'data1 -> ql.data1, 'data2 -> ql.data2, 'data3 -> ql.data3, 'data4 -> ql.data4, 'data5 -> ql.data5).executeUpdate()
  }
}