package xivo.queuestats.model

import org.scalatest.{FlatSpec, Matchers}
import xivo.queuestats.dbunit.DBUtil

class CelSpec extends FlatSpec with Matchers {

  implicit val connection = DBUtil.conn
  DBUtil.setupDB("queuestats.xml")

  "The Cel singleton" should "return the linkedid associated to a uniqueid" in {
    CallOnQueueTestUtils.insertCel("123456.789", "123456.777")

    Cel.linkedIdFromUniqueId("123456.789") shouldEqual Some("123456.777")
    Cel.linkedIdFromUniqueId("12345.999") shouldEqual None
  }
}
