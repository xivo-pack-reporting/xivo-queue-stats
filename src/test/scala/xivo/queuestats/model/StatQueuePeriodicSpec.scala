package xivo.queuestats.model

import java.sql.Connection
import java.util.Date

import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.ConnectionFactory
import xivo.queuestats.dbunit.DBUtil
import anorm.SqlParser._
import anorm.{SQL, _}

class StatQueuePeriodicSpec extends FlatSpec with Matchers with BeforeAndAfterEach with BeforeAndAfterAll {
  var factory: ConnectionFactory = null

  implicit var connection: Connection = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val period = new Period(0, 15, 0, 0)

  override def beforeAll() {
    DBUtil.setupDB("queuestats.xml")
    factory = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD)
    connection = factory.getConnection
  }

  override def beforeEach() {
    List("call_on_queue", "stat_queue_periodic").foreach(s => DBUtil.cleanTable(s))
  }
  "The StatQueuePeriodic singleton" should "fill stat_queue_periodic from call_on_queue with a given time interval" in {
    val call1 = CallOnQueue(None, "123456.789", format.parseDateTime("2013-01-01 07:52:00").toDate, 5,
      Some(format.parseDateTime("2013-01-01 07:52:05").toDate), Some(format.parseDateTime("2013-01-01 07:57:05").toDate),
      Some(CallExitType.Answered), "test", Some("2000"))
    val call2 = CallOnQueue(None, "123456.799", format.parseDateTime("2013-01-01 07:59:00").toDate, 5,
      Some(format.parseDateTime("2013-01-01 08:01:05").toDate), Some(format.parseDateTime("2013-01-01 01:06:05").toDate),
      Some(CallExitType.Answered), "test", Some("2000"))
    val call3 = CallOnQueue(None, "123456.811", format.parseDateTime("2013-01-01 08:02:00").toDate, 30,
      Some(format.parseDateTime("2013-01-01 08:02:05").toDate), Some(format.parseDateTime("2013-01-01 08:07:05").toDate),
      Some(CallExitType.Abandoned), "test", Some("2000"))
    val call4 = CallOnQueue(None, "123456.812", format.parseDateTime("2013-01-01 08:03:00").toDate, 30,
      Some(format.parseDateTime("2013-01-01 08:03:05").toDate), Some(format.parseDateTime("2013-01-01 08:08:05").toDate),
      Some(CallExitType.Abandoned), "test", Some("2000"))
    val call5 = CallOnQueue(None, "123456.813", format.parseDateTime("2013-01-01 08:04:00").toDate, 5,
      Some(format.parseDateTime("2013-01-01 08:04:05").toDate), Some(format.parseDateTime("2013-01-01 08:09:05").toDate),
      Some(CallExitType.Answered), "test", Some("2000"))
    val call6 = CallOnQueue(None, "123456.814", format.parseDateTime("2013-01-01 08:05:00").toDate, 5,
      Some(format.parseDateTime("2013-01-01 08:05:05").toDate), Some(format.parseDateTime("2013-01-01 08:10:05").toDate),
      Some(CallExitType.DivertCaRatio), "test", Some("2000"))
    val call7 = CallOnQueue(None, "123456.815", format.parseDateTime("2013-01-01 08:06:00").toDate, 5,
      Some(format.parseDateTime("2013-01-01 08:06:05").toDate), Some(format.parseDateTime("2013-01-01 08:11:05").toDate),
      Some(CallExitType.DivertWaitTime), "test", Some("2000"))
    val call8 = CallOnQueue(None, "123456.816", format.parseDateTime("2013-01-01 08:07:00").toDate, 5,
      Some(format.parseDateTime("2013-01-01 08:07:05").toDate), Some(format.parseDateTime("2013-01-01 08:12:05").toDate),
      Some(CallExitType.Timeout), "test", Some("2000"))
    val call9 = CallOnQueue(None, "123456.817", format.parseDateTime("2013-01-01 08:09:00").toDate, 5,
      Some(format.parseDateTime("2013-01-01 08:09:05").toDate), Some(format.parseDateTime("2013-01-01 08:14:05").toDate),
      Some(CallExitType.JoinEmpty), "test", Some("2000"))
    val call10 = CallOnQueue(None, "123456.818", format.parseDateTime("2013-01-01 08:10:00").toDate, 5,
      Some(format.parseDateTime("2013-01-01 08:10:05").toDate), Some(format.parseDateTime("2013-01-01 08:15:05").toDate),
      Some(CallExitType.LeaveEmpty), "test", Some("2000"))
    val call11 = CallOnQueue(None, "123456.819", format.parseDateTime("2013-01-01 08:11:00").toDate, 5,
      Some(format.parseDateTime("2013-01-01 08:11:05").toDate), Some(format.parseDateTime("2013-01-01 08:16:05").toDate),
      Some(CallExitType.Full), "test", Some("2000"))
    val call12 = CallOnQueue(None, "123456.820", format.parseDateTime("2013-01-01 08:12:00").toDate, 5,
      Some(format.parseDateTime("2013-01-01 08:12:05").toDate), Some(format.parseDateTime("2013-01-01 08:17:05").toDate),
      Some(CallExitType.Closed), "test", Some("2000"))
    CallOnQueue.insert(List(call1, call2, call3, call4, call5, call6, call7, call8, call9, call10, call11, call12))

    val start = format.parseDateTime("2013-01-01 07:45:00")
    val end = format.parseDateTime("2013-01-01 08:15:00")
    StatQueuePeriodic.insertFromCallOnQueue(start, end, period)
    StatQueuePeriodic.insertFromCallOnQueue(format.parseDateTime("2013-01-01 08:15:00"), format.parseDateTime("2013-01-01 08:45:00"), period)

    StatQueuePeriodicTestUtils.allStatQueuePeriodic shouldEqual List(
    StatQueuePeriodic(None, format.parseDateTime("2013-01-01 07:45:00").toDate, "test", 1, 0, 2, 0, 0, 0, 0, 0, 0, 0),
    StatQueuePeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "test", 2, 2, 10, 1, 1, 1, 1, 1, 1, 1))
  }


  it should "update old stat_queue_periodic entries with formerly pending but now finished calls, except for answered calls" in {
    val qp1 = StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00").toDate, "queue01", 2, 3, 6, 1, 0, 0, 0, 0, 0, 0)
    val qp2 = StatQueuePeriodic(None, format.parseDateTime("2014-01-01 09:00:00").toDate, "queue01", 1, 4, 6, 1, 0, 0, 0, 0, 0, 0)
    val callClosed = CallOnQueue(None, "123456.820", format.parseDateTime("2014-01-01 08:12:00").toDate, 5,
      None, Some(format.parseDateTime("2014-01-01 08:17:05").toDate),
      Some(CallExitType.Closed), "queue01", Some("2000"))
    val callAnswered = CallOnQueue(None, "123456.821", format.parseDateTime("2014-01-01 08:12:00").toDate, 5,
      Some(format.parseDateTime("2014-01-01 08:17:05").toDate), Some(format.parseDateTime("2014-01-01 08:19:05").toDate),
      Some(CallExitType.Answered), "queue01", Some("2000"))
    StatQueuePeriodicTestUtils.insertStatQueuePeriodic(qp1)
    StatQueuePeriodicTestUtils.insertStatQueuePeriodic(qp2)

    StatQueuePeriodic.updateFormerStats(callClosed, period)
    StatQueuePeriodic.updateFormerStats(callAnswered, period)

    StatQueuePeriodicTestUtils.allStatQueuePeriodic() shouldEqual List(
      StatQueuePeriodic(None, format.parseDateTime("2014-01-01 08:00:00").toDate, "queue01", 2, 3, 6, 1, 1, 0, 0, 0, 0, 0),
      qp2)
  }

  it should "insert 0 instead if null values" in {
    val call = CallOnQueue(None, "123456.811", format.parseDateTime("2013-01-01 08:02:00").toDate, 30,
      Some(format.parseDateTime("2013-01-01 08:02:05").toDate), Some(format.parseDateTime("2013-01-01 08:07:05").toDate),
      Some(CallExitType.Abandoned), "test", Some("2000"))

    CallOnQueue.insert(List(call))

    val start = format.parseDateTime("2013-01-01 08:00:00")
    val end = format.parseDateTime("2013-01-01 08:15:00")
    StatQueuePeriodic.insertFromCallOnQueue(start, end, period)

    StatQueuePeriodicTestUtils.allStatQueuePeriodic shouldEqual List(
      StatQueuePeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "test", 0, 1, 1, 0, 0, 0, 0, 0, 0, 0))
  }
}

object StatQueuePeriodicTestUtils {
  private val simple = get[Date]("time") ~
    get[String]("queue") ~
    get[Int]("answered") ~
    get[Int]("abandoned") ~
    get[Int]("total") ~
    get[Int]("full") ~
    get[Int]("closed") ~
    get[Int]("joinempty") ~
    get[Int]("leaveempty") ~
    get[Int]("divert_ca_ratio") ~
    get[Int]("divert_waittime") ~
    get[Int]("timeout") map {
    case time ~ queue ~ answered ~ abandoned ~ total ~ full ~ closed ~ joinempty ~ leaveempty
      ~ divertcaratio ~ divertcawaittime ~ timeout =>
      StatQueuePeriodic(None, time, queue, answered, abandoned, total, full, closed, joinempty, leaveempty, divertcaratio, divertcawaittime, timeout)
  }


  def allStatQueuePeriodic()(implicit conn: Connection) = SQL("SELECT * FROM stat_queue_periodic ORDER BY time ASC").as(simple *)

  def insertStatQueuePeriodic(sqp: StatQueuePeriodic)(implicit conn: Connection) = SQL(
    """INSERT INTO stat_queue_periodic(time, queue, answered, abandoned, total, "full", closed, joinempty,
      leaveempty, divert_ca_ratio, divert_waittime, timeout) VALUES ({time}, {queue}, {answered}, {abandoned}, {total},
      {full}, {closed}, {joinempty}, {leaveempty}, {divert_ca_ratio}, {divert_waittime}, {timeout})""")
    .on('time -> sqp.time, 'queue -> sqp.queue, 'answered -> sqp.answered, 'abandoned -> sqp.abandoned,
      'total -> sqp.total, 'full -> sqp.full, 'closed -> sqp.closed, 'joinempty -> sqp.joinEmpty, 'leaveempty -> sqp.leaveEmpty,
      'divert_ca_ratio -> sqp.divertCaRatio, 'divert_waittime -> sqp.divertWaitTime, 'timeout -> sqp.timeout)
    .executeUpdate()
}