package xivo.queuestats.model

import java.sql.Connection

import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.ConnectionFactory
import xivo.queuestats.dbunit.DBUtil

class TimedTableSpec extends FlatSpec with Matchers with BeforeAndAfterEach with BeforeAndAfterAll {

  var factory: ConnectionFactory = null
  implicit var connection: Connection = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val timedTable = new TimedTable("stat_agent_periodic", "time")

  override def beforeAll() {
    DBUtil.setupDB("queuestats.xml")
    factory = new ConnectionFactory(DBUtil.HOST, DBUtil.DB_NAME, DBUtil.USER, DBUtil.PASSWORD)
    connection = factory.getConnection
  }

  override def beforeEach() {
    DBUtil.cleanTable("stat_agent_periodic")
  }

  "A TimedTable" should "get the max date" in {
    val stat1 = StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "2000", 3600, 600, 100)
    val stat2 = StatAgentPeriodic(None, format.parseDateTime("2013-01-01 09:00:00").toDate, "2000", 3600, 600, 100)
    val stat3 = StatAgentPeriodic(None, format.parseDateTime("2013-01-01 10:00:00").toDate, "2000", 3600, 600, 100)
    StatAgentPeriodic.insertAll(List(stat1, stat2, stat3))

    timedTable.getMaxDate() shouldEqual Some(format.parseDateTime("2013-01-01 10:00:00"))
  }

  it should "return None for the max date if there is no data" in {
    timedTable.getMaxDate() shouldBe None
  }

  it should "get the min date" in {
    val stat1 = StatAgentPeriodic(None, format.parseDateTime("2013-01-01 08:00:00").toDate, "2000", 3600, 600, 100)
    val stat2 = StatAgentPeriodic(None, format.parseDateTime("2013-01-01 09:00:00").toDate, "2000", 3600, 600, 100)
    val stat3 = StatAgentPeriodic(None, format.parseDateTime("2013-01-01 10:00:00").toDate, "2000", 3600, 600, 100)
    StatAgentPeriodic.insertAll(List(stat1, stat2, stat3))

    timedTable.getMinDate() shouldEqual Some(format.parseDateTime("2013-01-01 08:00:00"))
  }

  it should "return None for the min date if there is no data" in {
    timedTable.getMaxDate() shouldBe None
  }
}

