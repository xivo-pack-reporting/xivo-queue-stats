package xivo.queuestats.parser

import org.easymock.EasyMock
import org.scalatest.{BeforeAndAfterEach, Matchers, FlatSpec}
import xivo.queuestats.model.{QueueLog, CallOnQueue}
import org.easymock.EasyMock.createNiceMock
import xivo.queuestats.dbunit.DBUtil

class CallStateMachineSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  var machine: CallStateMachine = null
  implicit val c = DBUtil.conn

  override def beforeEach() {
    machine = new CallStateMachine
  }

  "A CallStateMachine" should "get the CallOnQueue from its internal state" in {
    val state = createNiceMock(classOf[CallState])
    val call = createNiceMock(classOf[CallOnQueue])
    EasyMock.expect(state.getCallOnQueue).andReturn(Some(call))
    EasyMock.replay(state)
    machine.state = state

    machine.getCallOnQueue.get shouldBe theSameInstanceAs(call)
    EasyMock.verify(state)
  }

  it should "ask the next state to its own internal state" in {
    val currentState = createNiceMock(classOf[CallState])
    val nextState = createNiceMock(classOf[CallState])
    val ql = EasyMock.createNiceMock(classOf[QueueLog])
    EasyMock.expect(currentState.parseQueueLog(ql)).andReturn(nextState)
    EasyMock.replay(currentState)
    machine.state = currentState

    machine.parseQueueLog(ql)

    machine.state shouldBe theSameInstanceAs(nextState)
    EasyMock.verify(currentState)
  }
}
