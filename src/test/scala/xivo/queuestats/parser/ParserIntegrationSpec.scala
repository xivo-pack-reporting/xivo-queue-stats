package xivo.queuestats.parser

import org.easymock.EasyMock
import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.model.{CallOnQueueTestUtils, CallExitType, CallOnQueue, QueueLog}
import org.easymock.EasyMock.createNiceMock
import xivo.queuestats.dbunit.DBUtil

class ParserIntegrationSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val callid1 = "123456.789"
  val callid2 = "123789.456"
  var parser: QueueLogParser = null
  implicit val c = DBUtil.conn
  CallOnQueueTestUtils.insertCel(callid1, callid1)
  CallOnQueueTestUtils.insertCel(callid2, callid2)

  override def beforeEach() {
    parser = new QueueLogParser
  }

  "The QueueLogParser class" should "parse abandoned calls" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00").toDate, callid1, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:05:00").toDate, callid1, "queue01", "NONE", "ABANDON", None, None, Some("300"), None, None)

    parser.parseQueueLogs(List(ql1, ql2)) shouldEqual Some(CallOnQueue(None, callid1, format.parseDateTime("2013-01-01 08:00:00").toDate, 0, None,
      Some(format.parseDateTime("2013-01-01 08:05:00").toDate), Some(CallExitType.Abandoned), "queue01", None))
  }

  it should "parse timeout calls" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00").toDate, callid1, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:05:00").toDate, callid1, "queue01", "NONE", "EXITWITHTIMEOUT", None, None, Some("300"), None, None)

    parser.parseQueueLogs(List(ql1, ql2)) shouldEqual Some(CallOnQueue(None, callid1, format.parseDateTime("2013-01-01 08:00:00").toDate, 0, None,
      Some(format.parseDateTime("2013-01-01 08:05:00").toDate), Some(CallExitType.Timeout), "queue01", None))
  }

  it should "parse calls on a closed queue" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00").toDate, callid1, "queue01", "NONE", "CLOSED", None, None, None, None, None)

    parser.parseQueueLogs(List(ql1)) shouldEqual Some(CallOnQueue(None, callid1, format.parseDateTime("2013-01-01 08:00:00").toDate, 0, None,
      Some(format.parseDateTime("2013-01-01 08:00:00").toDate), Some(CallExitType.Closed), "queue01", None))
  }

  it should "parse answered calls" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00").toDate, callid1, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:05:00").toDate, callid1, "queue01", "Agent/2000", "CONNECT", Some("300"), Some(callid2), Some("8"), None, None)
    val ql3 = QueueLog(3, format.parseDateTime("2013-01-01 08:07:30").toDate, callid1, "queue01", "Agent/2000", "COMPLETEAGENT", Some("300"), Some("240"), Some("3"), None, None)

    parser.parseQueueLogs(List(ql1, ql2, ql3)) shouldEqual Some(CallOnQueue(None, callid1, format.parseDateTime("2013-01-01 08:00:00").toDate, 8,
      Some(format.parseDateTime("2013-01-01 08:05:00").toDate), Some(format.parseDateTime("2013-01-01 08:07:30").toDate), Some(CallExitType.Answered), "queue01", Some("2000")))
  }

  it should "not fail and return an empty list if a transition raises an exception" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00").toDate, callid1, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val machine = createNiceMock(classOf[CallStateMachine])
    parser.callStateMachine = machine
    EasyMock.expect(machine.parseQueueLog(ql1)).andThrow(new Exception("So what ?"))
    EasyMock.replay(machine)

    parser.parseQueueLogs(List(ql1)) shouldEqual None
  }

  "The QueueLogParser object" should "parse non-consecutive QueueLogs of different calls" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00").toDate, callid1, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:01:00").toDate, callid2, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql3 = QueueLog(3, format.parseDateTime("2013-01-01 08:05:00").toDate, callid1, "queue01", "NONE", "ABANDON", None, None, Some("300"), None, None)
    val ql4 = QueueLog(4, format.parseDateTime("2013-01-01 08:06:00").toDate, callid2, "queue01", "NONE", "EXITWITHTIMEOUT", None, None, Some("300"), None, None)

    val res = QueueLogParser.parseAllQueueLogs(List(ql1, ql2, ql3, ql4))

    res.size shouldEqual 2
    val call1 = CallOnQueue(None, callid1, format.parseDateTime("2013-01-01 08:00:00").toDate, 0, None,
      Some(format.parseDateTime("2013-01-01 08:05:00").toDate), Some(CallExitType.Abandoned), "queue01", None)
    val call2 = CallOnQueue(None, callid2, format.parseDateTime("2013-01-01 08:01:00").toDate, 0, None,
      Some(format.parseDateTime("2013-01-01 08:06:00").toDate), Some(CallExitType.Timeout), "queue01", None)
    res shouldEqual List(call1, call2)
  }

  it should "extract 2 CallOnQueue when the same call was distributed on 2 queues" in {
    val ql1 = QueueLog(1, format.parseDateTime("2013-01-01 08:00:00").toDate, callid1, "queue01", "NONE", "CLOSED", None, None, None, None, None)
    val ql2 = QueueLog(2, format.parseDateTime("2013-01-01 08:00:01").toDate, callid1, "queue02", "NONE", "ENTERQUEUE", None, None, None, None, None)
    val ql3 = QueueLog(3, format.parseDateTime("2013-01-01 08:05:01").toDate, callid1, "queue02", "NONE", "ABANDON", None, None, Some("300"), None, None)

    val res = QueueLogParser.parseAllQueueLogs(List(ql1, ql2, ql3))

    res.size shouldEqual 2
    val call1 = CallOnQueue(None, callid1, format.parseDateTime("2013-01-01 08:00:00").toDate, 0, None, Some(format.parseDateTime("2013-01-01 08:00:00").toDate), Some(CallExitType.Closed), "queue01", None)
    val call2 = CallOnQueue(None, callid1, format.parseDateTime("2013-01-01 08:00:01").toDate, 0, None,
      Some(format.parseDateTime("2013-01-01 08:05:01").toDate), Some(CallExitType.Abandoned), "queue02", None)
    res shouldEqual List(call1, call2)
  }

  it should "detect starting events" in {
    QueueLogParser.isStartingEvent("ENTERQUEUE") shouldBe true
    QueueLogParser.isStartingEvent("CLOSED") shouldBe true
    QueueLogParser.isStartingEvent("DIVERT_CA_RATIO") shouldBe true
    QueueLogParser.isStartingEvent("DIVERT_HOLDTIME") shouldBe true
    QueueLogParser.isStartingEvent("FULL") shouldBe true
    QueueLogParser.isStartingEvent("JOINEMPTY") shouldBe true
    QueueLogParser.isStartingEvent("CONNECT") shouldBe false
  }
}

