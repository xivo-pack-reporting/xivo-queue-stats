package xivo.queuestats.parser

import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.model.{CallExitType, CallOnQueue, QueueLog}

class QueuedSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  var state: Queued = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val callid = "123456.789"
  var call: CallOnQueue = null

  override def beforeEach() {
    call = CallOnQueue(None, callid, format.parseDateTime("2013-01-01 08:00:00").toDate, 0, None, None, None, "queue01", None)
    state = new Queued(call)
  }

  "The Queued" should "set the waitTime, the status to Abandoned and return Finished on ABANDON" in {
    val ql = QueueLog(1, format.parseDateTime("2013-01-01 08:04:00").toDate, callid, "queue01", "NONE", "ABANDON", None, None, Some("240"), None, None)

    val res = state.parseQueueLog(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getCallOnQueue shouldEqual
      Some(CallOnQueue(None, callid, format.parseDateTime("2013-01-01 08:00:00").toDate, 0, None, Some(format.parseDateTime("2013-01-01 08:04:00").toDate),
        Some(CallExitType.Abandoned), "queue01", None))
  }

  it should "set the waitTime, the status to Timeout and return Finished on EXITWITHTIMEOUT" in {
    val ql = QueueLog(1, format.parseDateTime("2013-01-01 08:04:00").toDate, callid, "queue01", "NONE", "EXITWITHTIMEOUT", None, None, Some("240"), None, None)

    val res = state.parseQueueLog(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getCallOnQueue shouldEqual
      Some(CallOnQueue(None, callid, format.parseDateTime("2013-01-01 08:00:00").toDate, 0, None,
        Some(format.parseDateTime("2013-01-01 08:04:00").toDate), Some(CallExitType.Timeout), "queue01", None))
  }

  it should "accumulate the ringTime, set the waitTime, the status to Answered and return Answered on CONNECT" in {
    call.ringSeconds = 30
    val ql = QueueLog(1, format.parseDateTime("2013-01-01 08:05:00").toDate, callid, "queue01", "Agent/2000", "CONNECT",
      Some("300"), Some(callid), Some("3"), None, None)

    val res = state.parseQueueLog(ql)

    res.getClass shouldEqual classOf[Answered]
    res.getCallOnQueue shouldEqual
      Some(CallOnQueue(None, callid, format.parseDateTime("2013-01-01 08:00:00").toDate, 33, Some(format.parseDateTime("2013-01-01 08:05:00").toDate),
        None, Some(CallExitType.Answered), "queue01", Some("2000")))
  }

  it should "set the waitTime, the status to LeaveEmpty and return Finished on LEAVEEMPTY" in {
    val ql = QueueLog(1, format.parseDateTime("2013-01-01 08:04:00").toDate, callid, "queue01", "NONE", "LEAVEEMPTY", None, None, None, None, None)

    val res = state.parseQueueLog(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getCallOnQueue shouldEqual
      Some(CallOnQueue(None, callid, format.parseDateTime("2013-01-01 08:00:00").toDate, 0, None, Some(format.parseDateTime("2013-01-01 08:04:00").toDate),
        Some(CallExitType.LeaveEmpty), "queue01", None))
  }

  it should "accumulate the ringTime on RINGNOANSWER" in {
    call.ringSeconds = 25
    val ql = QueueLog(1, format.parseDateTime("2013-01-01 08:04:00").toDate, callid, "queue01", "Agent/2000", "RINGNOANSWER", Some("15000"), None, None, None, None)

    val res = state.parseQueueLog(ql)

    res.getClass shouldEqual classOf[Queued]
    res.getCallOnQueue shouldEqual
      Some(CallOnQueue(None, callid, format.parseDateTime("2013-01-01 08:00:00").toDate, 40, None, None, None, "queue01", None))
  }

  it should "extract the agent number from a string if it matches Agent/\\d+" in {
    val str = "Agent/5014"
    state.extractAgentNumber(str) should equal("5014")
  }

  it should "return the full string if it doesn't" in {
    val str = "stuff"
    state.extractAgentNumber(str) should equal("stuff")
  }
}
