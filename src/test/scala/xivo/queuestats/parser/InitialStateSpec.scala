package xivo.queuestats.parser

import org.joda.time.format.DateTimeFormat
import org.scalatest.{BeforeAndAfterEach, FlatSpec, Matchers}
import xivo.queuestats.model.{CallOnQueueTestUtils, CallExitType, CallOnQueue, QueueLog}
import xivo.queuestats.dbunit.DBUtil

class InitialStateSpec extends FlatSpec with Matchers with BeforeAndAfterEach {

  var state: InitialState = null
  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  val callid = "123456.789"
  implicit val c = DBUtil.conn
  DBUtil.setupDB("queuestats.xml")
  CallOnQueueTestUtils.insertCel(callid, callid)

  override def beforeEach() {
    state = new InitialState
  }

  "The InitialState" should "set the time, queue and callid and return Queued on ENTERQUEUE" in {
    val date = format.parseDateTime("2013-01-01 08:00:00").toDate
    val ql = QueueLog(1, date, callid, "queue01", "NONE", "ENTERQUEUE", None, None, None, None, None)

    val res = state.parseQueueLog(ql)

    res.getClass shouldEqual classOf[Queued]
    res.getCallOnQueue shouldEqual Some(CallOnQueue(None, callid, date, 0, None, None, None, "queue01", None))
  }

  it should "set the time, queue, status and callid and return Finished on CLOSED" in {
    val date = format.parseDateTime("2013-01-01 08:00:00").toDate
    val ql = QueueLog(1, date, callid, "queue01", "NONE", "CLOSED", None, None, None, None, None)

    val res = state.parseQueueLog(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getCallOnQueue shouldEqual Some(CallOnQueue(None, callid, date, 0, None, Some(date), Some(CallExitType.Closed), "queue01", None))
  }

  it should "set the time, queue, status and callid and return Finished on FULL" in {
    val date = format.parseDateTime("2013-01-01 08:00:00").toDate
    val ql = QueueLog(1, date, callid, "queue01", "NONE", "FULL", None, None, None, None, None)

    val res = state.parseQueueLog(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getCallOnQueue shouldEqual Some(CallOnQueue(None, callid, date, 0, None, Some(date), Some(CallExitType.Full), "queue01", None))
  }

  it should "set the time, queue, status and callid and return Finished on JOINEMPTY" in {
    val date = format.parseDateTime("2013-01-01 08:00:00").toDate
    val ql = QueueLog(1, date, callid, "queue01", "NONE", "JOINEMPTY", None, None, None, None, None)

    val res = state.parseQueueLog(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getCallOnQueue shouldEqual Some(CallOnQueue(None, callid, date, 0, None, Some(date), Some(CallExitType.JoinEmpty), "queue01", None))
  }

  it should "set the time, queue, status and callid and return Finished on DIVERT_CA_RATIO" in {
    val date = format.parseDateTime("2013-01-01 08:00:00").toDate
    val ql = QueueLog(1, date, callid, "queue01", "NONE", "DIVERT_CA_RATIO", None, None, None, None, None)

    val res = state.parseQueueLog(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getCallOnQueue shouldEqual Some(CallOnQueue(None, callid, date, 0, None, Some(date), Some(CallExitType.DivertCaRatio), "queue01", None))
  }

  it should "set the time, queue, status and callid and return Finished on DIVERT_HOLDTIME" in {
    val date = format.parseDateTime("2013-01-01 08:00:00").toDate
    val ql = QueueLog(1, date, callid, "queue01", "NONE", "DIVERT_HOLDTIME", None, None, None, None, None)

    val res = state.parseQueueLog(ql)

    res.getClass shouldEqual classOf[Finished]
    res.getCallOnQueue shouldEqual Some(CallOnQueue(None, callid, date, 0, None, Some(date), Some(CallExitType.DivertWaitTime), "queue01", None))
  }

  it should "throw an exception if there is no CEL associated with the queue_log" in {
    val date = format.parseDateTime("2013-01-01 08:00:00").toDate
    val ql = QueueLog(1, date, callid + "3", "queue01", "NONE", "DIVERT_HOLDTIME", None, None, None, None, None)

    an[NoSuchCelException] should be thrownBy state.parseQueueLog(ql)
  }

}
