package xivo.queuestats

import org.joda.time.Period
import org.joda.time.format.DateTimeFormat
import org.scalatest.{FlatSpec, Matchers}

class TimeUtilsSpec extends FlatSpec with Matchers {

  val format = DateTimeFormat.forPattern("YYYY-MM-dd HH:mm:ss")
  // intervalle de 15 minutes
  val interval = new Period(0, 15, 0, 0)

  "TimeUtils" should "round times to the previous interval" in {
    val time = format.parseDateTime("2014-01-01 08:23:54")
    TimeUtils.roundToInterval(time, interval) shouldEqual format.parseDateTime("2014-01-01 08:15:00")
  }

  it should "not modify already rounded times" in {
    val time = format.parseDateTime("2014-01-01 08:15:00")
    TimeUtils.roundToInterval(time, interval) shouldEqual time
  }

  it should "set milliseconds to 0" in {
    val time = format.parseDateTime("2014-01-01 08:23:54").withMillisOfSecond(200)
    TimeUtils.roundToInterval(time, interval) shouldEqual format.parseDateTime("2014-01-01 08:15:00")
  }

  it should "parse the interval string" in {
    TimeUtils.parseInterval("02:00:00") shouldEqual new Period(2, 0, 0, 0)
  }

}
